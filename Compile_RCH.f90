!  Compile_RCH.f90 
!
!  FUNCTIONS:
!  Compile_RCH - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: Compile_RCH
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

subroutine Compile_RCH
    use PARAM, only: startyr, endyr, OUTDIR, YRT, iyear, openoutfile, proj_name, output_unit, cyr, cYRT
    IMPLICIT NONE
    
    
    
    CHARACTER*1     Line(190)
    
    INTEGER     vln, i
    WRITE (output_unit, '(A)') 'COMPRCH'
   
    call  openoutfile (1, OUTDIR, trim(proj_name)//'.rch')
    WRITE (1, 100)
    WRITE (1, 101)
    WRITE (1, 102)
    
    DO iyear = startyr, endyr
        WRITE(output_unit, '(I6, I8)') iyear, cyr(iyear)
        WRITE(YRT, '(I4)') iyear
        
        OPEN (10, FILE = TRIM(OUTDIR)//'RCH\UNWNRD'//TRIM(YRT)//'.RCH', STATUS = 'OLD')
        
        DO WHILE (.NOT. EOF(10))
            READ (10, '(190A1)') Line(:)
            DO i = 190, 1, -1
                IF (Line(i) .NE. " ") THEN
                    vln = i
                    GOTO 1
                END IF
            END DO
1           WRITE (1, 105) Line(1 : vln)
        END DO
        CLOSE (10)

    END DO
        
100 FORMAT ('#MODFLOW2000 Recharge Package')	!Header for RCH file (File 20)
101 FORMAT ('PARAMETER  0')
102 FORMAT ('         3        40') !Header for RCH file (File 20)
105 FORMAT (<vln>A1)
    
END subroutine Compile_RCH

