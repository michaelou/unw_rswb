!  WSPP_Report.f90 
!
!  FUNCTIONS:
!  WSPP_Report - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: WSPP_Report
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************
MODULE parReport
    
    use Param, only: ncrop, nsoil, nCounty, nzones, rzones

    DOUBLE PRECISION    rPrecip(0 : 12, ncrop, 4, nsoil)
    DOUBLE PRECISION    rAcs(0 : 12, ncrop, 4, nsoil)
    DOUBLE PRECISION    rAP(0 : 12, ncrop, 4, nsoil)
    DOUBLE PRECISION    rSWD(0 : 12, ncrop, 4, nsoil)
    DOUBLE PRECISION    rET(0 : 12, ncrop, 4, nsoil)
    DOUBLE PRECISION    rRO(0 : 12, ncrop, 4, nsoil)
    DOUBLE PRECISION    rDP(0 : 12, ncrop, 4, nsoil)
    DOUBLE PRECISION    rSL(0 : 12, ncrop, 4, nsoil)
    DOUBLE PRECISION    rPSL(0 : 12, ncrop, 4, nsoil)
    DOUBLE PRECISION    rDAP(0 : 12, ncrop, 4, nsoil)
    DOUBLE PRECISION    rETG(0 : 12, ncrop, 4, nsoil)
    DOUBLE PRECISION    rDET(0 : 12, ncrop, 4, nsoil)
    DOUBLE PRECISION    rETB(0 : 12, ncrop, 4, nsoil)
    DOUBLE PRECISION    rDP1(0 : 12, ncrop, 4, nsoil)
    DOUBLE PRECISION    rDP2(0 : 12, ncrop, 4, nsoil)
    DOUBLE PRECISION    rRO1(0 : 12, ncrop, 4, nsoil)
    DOUBLE PRECISION    rRO2(0 : 12, ncrop, 4, nsoil)
    DOUBLE PRECISION    rETtrans(0 : 12, ncrop, 4, nsoil)
    DOUBLE PRECISION    rDP2RO(0 : 12, ncrop, 4, nsoil)
    DOUBLE PRECISION    rSF(0 : 12, ncrop, 4, nsoil)
    DOUBLE PRECISION    rRO2DP(0 : 12, ncrop, 4, nsoil)
    DOUBLE PRECISION    rRO2ET(0 : 12, ncrop, 4, nsoil)
    
    DOUBLE PRECISION    cPrecip(0 : 12, ncrop, 4, nsoil, ncounty)
    DOUBLE PRECISION    cAcs(0 : 12, ncrop, 4, nsoil, ncounty)
    DOUBLE PRECISION    cAP(0 : 12, ncrop, 4, nsoil, ncounty)
    DOUBLE PRECISION    cSWD(0 : 12, ncrop, 4, nsoil, ncounty)
    DOUBLE PRECISION    cET(0 : 12, ncrop, 4, nsoil, ncounty)
    DOUBLE PRECISION    cRO(0 : 12, ncrop, 4, nsoil, ncounty)
    DOUBLE PRECISION    cDP(0 : 12, ncrop, 4, nsoil, ncounty)
    DOUBLE PRECISION    cSL(0 : 12, ncrop, 4, nsoil, ncounty)
    DOUBLE PRECISION    cPSL(0 : 12, ncrop, 4, nsoil, ncounty)
    DOUBLE PRECISION    cDAP(0 : 12, ncrop, 4, nsoil, ncounty)
    DOUBLE PRECISION    cETG(0 : 12, ncrop, 4, nsoil, ncounty)
    DOUBLE PRECISION    cDET(0 : 12, ncrop, 4, nsoil, ncounty)
    DOUBLE PRECISION    cETB(0 : 12, ncrop, 4, nsoil, ncounty)
    DOUBLE PRECISION    cDP1(0 : 12, ncrop, 4, nsoil, ncounty)
    DOUBLE PRECISION    cDP2(0 : 12, ncrop, 4, nsoil, ncounty)
    DOUBLE PRECISION    cRO1(0 : 12, ncrop, 4, nsoil, ncounty)
    DOUBLE PRECISION    cRO2(0 : 12, ncrop, 4, nsoil, ncounty)
    DOUBLE PRECISION    cETtrans(0 : 12, ncrop, 4, nsoil, ncounty)
    DOUBLE PRECISION    cDP2RO(0 : 12, ncrop, 4, nsoil, ncounty)
    DOUBLE PRECISION    cSF(0 : 12, ncrop, 4, nsoil, ncounty)
    DOUBLE PRECISION    cRO2DP(0 : 12, ncrop, 4, nsoil, ncounty)
    DOUBLE PRECISION    cRO2ET(0 : 12, ncrop, 4, nsoil, ncounty)
    
    DOUBLE PRECISION    cfPrecip(0 : 12, ncrop, 4, nsoil, nzones)
    DOUBLE PRECISION    cfAcs(0 : 12, ncrop, 4, nsoil, nzones)
    DOUBLE PRECISION    cfAP(0 : 12, ncrop, 4, nsoil, nzones)
    DOUBLE PRECISION    cfSWD(0 : 12, ncrop, 4, nsoil, nzones)
    DOUBLE PRECISION    cfET(0 : 12, ncrop, 4, nsoil, nzones)
    DOUBLE PRECISION    cfRO(0 : 12, ncrop, 4, nsoil, nzones)
    DOUBLE PRECISION    cfDP(0 : 12, ncrop, 4, nsoil, nzones)
    DOUBLE PRECISION    cfSL(0 : 12, ncrop, 4, nsoil, nzones)
    DOUBLE PRECISION    cfPSL(0 : 12, ncrop, 4, nsoil, nzones)
    DOUBLE PRECISION    cfDAP(0 : 12, ncrop, 4, nsoil, nzones)
    DOUBLE PRECISION    cfETG(0 : 12, ncrop, 4, nsoil, nzones)
    DOUBLE PRECISION    cfDET(0 : 12, ncrop, 4, nsoil, nzones)
    DOUBLE PRECISION    cfETB(0 : 12, ncrop, 4, nsoil, nzones)
    DOUBLE PRECISION    cfDP1(0 : 12, ncrop, 4, nsoil, nzones)
    DOUBLE PRECISION    cfDP2(0 : 12, ncrop, 4, nsoil, nzones)
    DOUBLE PRECISION    cfRO1(0 : 12, ncrop, 4, nsoil, nzones)
    DOUBLE PRECISION    cfRO2(0 : 12, ncrop, 4, nsoil, nzones)
    DOUBLE PRECISION    cfETtrans(0 : 12, ncrop, 4, nsoil, nzones)
    DOUBLE PRECISION    cfDP2RO(0 : 12, ncrop, 4, nsoil, nzones)
    DOUBLE PRECISION    cfSF(0 : 12, ncrop, 4, nsoil, nzones)
    DOUBLE PRECISION    cfRO2DP(0 : 12, ncrop, 4, nsoil, nzones)
    DOUBLE PRECISION    cfRO2ET(0 : 12, ncrop, 4, nsoil, nzones)

    DOUBLE PRECISION    rzPrecip(0 : 12, ncrop, 4, nsoil, 0 : rzones)
    DOUBLE PRECISION    rzAcs(0 : 12, ncrop, 4, nsoil, 0 : rzones)
    DOUBLE PRECISION    rzAP(0 : 12, ncrop, 4, nsoil, 0 : rzones)
    DOUBLE PRECISION    rzSWD(0 : 12, ncrop, 4, nsoil, 0 : rzones)
    DOUBLE PRECISION    rzET(0 : 12, ncrop, 4, nsoil, 0 : rzones)
    DOUBLE PRECISION    rzRO(0 : 12, ncrop, 4, nsoil, 0 : rzones)
    DOUBLE PRECISION    rzDP(0 : 12, ncrop, 4, nsoil, 0 : rzones)
    DOUBLE PRECISION    rzSL(0 : 12, ncrop, 4, nsoil, 0 : rzones)
    DOUBLE PRECISION    rzPSL(0 : 12, ncrop, 4, nsoil, 0 : rzones)
    DOUBLE PRECISION    rzDAP(0 : 12, ncrop, 4, nsoil, 0 : rzones)
    DOUBLE PRECISION    rzETG(0 : 12, ncrop, 4, nsoil, 0 : rzones)
    DOUBLE PRECISION    rzDET(0 : 12, ncrop, 4, nsoil, 0 : rzones)
    DOUBLE PRECISION    rzETB(0 : 12, ncrop, 4, nsoil, 0 : rzones)
    DOUBLE PRECISION    rzDP1(0 : 12, ncrop, 4, nsoil, 0 : rzones)
    DOUBLE PRECISION    rzDP2(0 : 12, ncrop, 4, nsoil, 0 : rzones)
    DOUBLE PRECISION    rzRO1(0 : 12, ncrop, 4, nsoil, 0 : rzones)
    DOUBLE PRECISION    rzRO2(0 : 12, ncrop, 4, nsoil, 0 : rzones)
    DOUBLE PRECISION    rzETtrans(0 : 12, ncrop, 4, nsoil, 0 : rzones)
    DOUBLE PRECISION    rzDP2RO(0 : 12, ncrop, 4, nsoil, 0 : rzones)
    DOUBLE PRECISION    rzSF(0 : 12, ncrop, 4, nsoil, 0 : rzones)
    DOUBLE PRECISION    rzRO2DP(0 : 12, ncrop, 4, nsoil, 0 : rzones)
    DOUBLE PRECISION    rzRO2ET(0 : 12, ncrop, 4, nsoil, 0 : rzones)    
    
END MODULE parReport
!*************************************************************************

PROGRAM WSPP_Report
    USE parReport
    use Param, only : h, i, j, k, l, m, n, pflag, &
                  yr, iyr, cyr, YRT, cYRT, startyr, endyr, &
                  cell, rzones, nzones, ncell, &
                  INDIR, OUTDIR, RAWDIR, WBPDIR, &
                  County, Cntyflag, CellCnty,Irrig, &
                  Crops, CoefZone, ROZone, Precip, &
                  DryETadj, imiss, rmiss, iyear, LPM, MiToGauge, PertoRchg, &
                  openoutfile, errexit, output_unit
                      
    use param2
    IMPLICIT NONE
    
    WRITE (output_unit, '(A)') 'REPORT'
    !Populate County Names
    OPEN (1, FILE = TRIM(INDIR)//TRIM(CntyID), STATUS = 'OLD')
    READ (1, *)
    DO WHILE (.NOT. EOF(1))
        READ (1, *) idx, geoid, CountyName(idx), cState(idx)
    END DO
    CLOSE (1)
    if (.not. allocated(Cntyflag)) call errexit('must define ZONEID before REPORT')  
    
    ActCounty = 0
    !Populate Cell-County Relationship
    OPEN (1, FILE = TRIM(INDIR)//TRIM(CntyCell), STATUS = 'OLD')
    READ (1, *)
    DO WHILE (.NOT. EOF(1))
        READ (1, *) Cell, geoid, cntyidx(cell)
        ActCounty(cntyidx(cell)) = 1
    END DO
    CLOSE (1)
    if (.not. allocated(CellCnty)) call errexit('must define ZONEID before REPORT')  
    
    !Populate Cell Location
    OPEN (1, FILE = TRIM(INDIR)//'CellLoc3mz.csv', STATUS = 'OLD')
    READ(1, *)
    DO WHILE (.NOT. EOF(1))
        READ (1, *) Cell, soil(cell), CoefZ(cell), ROZ(cell), M2G(cell)
    END DO
    CLOSE (1)
    
    !Load Cell Coefficients
    if (.not. allocated(DryETadj)) call errexit('must define COEFF before REPORT')
    
    !Populate Runoff Zone Coefficients
	OPEN (1, File = TRIM(INDIR)//'ROZoneCoef.txt', STATUS = 'OLD')
    READ (1, *)
	DO WHILE (.NOT. EOF(1))
		READ (1, *) izone, LPM(izone)
	END DO  
	Close (1)
    
    !Load Call Year Identification
    OPEN (1, FILE = TRIM(INDIR)//'CallYr.txt', STATUS = 'OLD')
    READ (1, *)
    DO WHILE (.NOT. EOF(1))
        READ (1, *) yr, iyr
        IF ((yr .GE. startyr) .AND. (yr .LE. endyr)) cyr(yr) = iyr
    END DO
    CLOSE (1)
    
    DO iyear = startyr, endyr
        WRITE (YRT, '(I4)') iyear
        WRITE (YRTc, '(I4)') cyr(iyear)
        WRITE(6, *) iyear, cyr(iyear)
        
        CALL zerovals
        
        Precip = 0.
        OPEN (1, FILE = TRIM(WBPDIR)//'Precip\Precip'//TRIM(YRTc)//'.txt', STATUS = 'OLD')
        READ (1, *)
        DO WHILE (.NOT. EOF(1))
            READ (1, *, END = 99) Cell, YR, icrop, Precip(Cell, 0 : 12)
        END DO
99      CLOSE (1)
        
        OPEN (1, FILE = TRIM(RAWDIR)//'RAW\RAW_WSPP'//TRIM(YRT)//'.txt', STATUS = 'OLD')
        READ (1, *)
    
        DO WHILE (.NOT. EOF(1))
            READ (1, *) Cell, Yr, Mon, icrop, isoil, Pflag, iczone, irzone, acs, AP, SWD, ET, RO, & !13
                                    DP, SL, PSL, DAP, ETG, DET, ETB, DP1, & !8
                                    DP2, RO1, RO2, EWat, ETtrans, NIRmET !6
            
            IF (AP .GT. 0.) THEN
                CONTINUE
            END IF
            
            isoil = soil(cell)
            iczone = CoefZ(cell)
            irzone = ROZ(cell)
            
            IF (M2G(Cell) .EQ. 0.) THEN
                LF = 0.5
            ELSE
                LF = MIN(1 - EXP(-LPM(ROZ(Cell)) * M2G(Cell)), 1.0)
            END IF
            
            SF = RO * (1 - LF)
            RO2DP = RO * LF * PctRch(CoefZ(Cell))
            RO2ET = RO * LF * (1 - PctRch(CoefZ(Cell)))
            
            IF (ABS(RO - SF - RO2DP - RO2ET) .GT. 0.0001) THEN
                CONTINUE
            END IF
            
            rPrecip(mon, icrop, pflag, isoil) = rPrecip(mon, icrop, pflag, isoil) + Precip(cell, mon) * acs / 12
            rAcs(mon, icrop, pflag, isoil) = rAcs(mon, icrop, pflag, isoil) + Acs
            rAP(mon, icrop, pflag, isoil) = rAP(mon, icrop, pflag, isoil) + AP
            rSWD(mon, icrop, pflag, isoil) = rSWD(mon, icrop, pflag, isoil) + SWD
            rET(mon, icrop, pflag, isoil) = rET(mon, icrop, pflag, isoil) + ET
            rRO(mon, icrop, pflag, isoil) = rRO(mon, icrop, pflag, isoil) + RO
            rDP(mon, icrop, pflag, isoil) = rDP(mon, icrop, pflag, isoil) + DP
            rSL(mon, icrop, pflag, isoil) = rSL(mon, icrop, pflag, isoil) + SL
            rPSL(mon, icrop, pflag, isoil) = rPSL(mon, icrop, pflag, isoil) + PSL
            rDAP(mon, icrop, pflag, isoil) = rDAP(mon, icrop, pflag, isoil) + DAP
            rETG(mon, icrop, pflag, isoil) = rETG(mon, icrop, pflag, isoil) + ETG
            rDET(mon, icrop, pflag, isoil) = rDET(mon, icrop, pflag, isoil) + DET
            rETB(mon, icrop, pflag, isoil) = rETB(mon, icrop, pflag, isoil) + ETB
            rDP1(mon, icrop, pflag, isoil) = rDP1(mon, icrop, pflag, isoil) + DP1
            rDP2(mon, icrop, pflag, isoil) = rDP2(mon, icrop, pflag, isoil) + DP2
            rRO1(mon, icrop, pflag, isoil) = rRO1(mon, icrop, pflag, isoil) + RO1
            rRO2(mon, icrop, pflag, isoil) = rRO2(mon, icrop, pflag, isoil) + RO2
            rETtrans(mon, icrop, pflag, isoil) = rETtrans(mon, icrop, pflag, isoil) + ETtrans
            rDP2RO(mon, icrop, pflag, isoil) = rDP2RO(mon, icrop, pflag, isoil) + DP2RO
            rSF(mon, icrop, pflag, isoil) = rSF(mon, icrop, pflag, isoil) + SF
            rRO2DP(mon, icrop, pflag, isoil) = rRO2DP(mon, icrop, pflag, isoil) + RO2DP
            rRO2ET(mon, icrop, pflag, isoil) = rRO2ET(mon, icrop, pflag, isoil) + RO2ET
            
            cPrecip(mon, icrop, pflag, isoil, cntyidx(cell)) = cPrecip(mon, icrop, pflag, isoil, cntyidx(cell)) + Precip(cell, mon) * acs / 12
            cAcs(mon, icrop, pflag, isoil, cntyidx(cell)) = cAcs(mon, icrop, pflag, isoil, cntyidx(cell)) + Acs
            cAP(mon, icrop, pflag, isoil, cntyidx(cell)) = cAP(mon, icrop, pflag, isoil, cntyidx(cell)) + AP
            cSWD(mon, icrop, pflag, isoil, cntyidx(cell)) = cSWD(mon, icrop, pflag, isoil, cntyidx(cell)) + SWD
            cET(mon, icrop, pflag, isoil, cntyidx(cell)) = cET(mon, icrop, pflag, isoil, cntyidx(cell)) + ET
            cRO(mon, icrop, pflag, isoil, cntyidx(cell)) = cRO(mon, icrop, pflag, isoil, cntyidx(cell)) + RO
            cDP(mon, icrop, pflag, isoil, cntyidx(cell)) = cDP(mon, icrop, pflag, isoil, cntyidx(cell)) + DP
            cSL(mon, icrop, pflag, isoil, cntyidx(cell)) = cSL(mon, icrop, pflag, isoil, cntyidx(cell)) + SL
            cPSL(mon, icrop, pflag, isoil, cntyidx(cell)) = cPSL(mon, icrop, pflag, isoil, cntyidx(cell)) + PSL
            cDAP(mon, icrop, pflag, isoil, cntyidx(cell)) = cDAP(mon, icrop, pflag, isoil, cntyidx(cell)) + DAP
            cETG(mon, icrop, pflag, isoil, cntyidx(cell)) = cETG(mon, icrop, pflag, isoil, cntyidx(cell)) + ETG
            cDET(mon, icrop, pflag, isoil, cntyidx(cell)) = cDET(mon, icrop, pflag, isoil, cntyidx(cell)) + DET
            cETB(mon, icrop, pflag, isoil, cntyidx(cell)) = cETB(mon, icrop, pflag, isoil, cntyidx(cell)) + ETB
            cDP1(mon, icrop, pflag, isoil, cntyidx(cell)) = cDP1(mon, icrop, pflag, isoil, cntyidx(cell)) + DP1
            cDP2(mon, icrop, pflag, isoil, cntyidx(cell)) = cDP2(mon, icrop, pflag, isoil, cntyidx(cell)) + DP2
            cRO1(mon, icrop, pflag, isoil, cntyidx(cell)) = cRO1(mon, icrop, pflag, isoil, cntyidx(cell)) + RO1
            cRO2(mon, icrop, pflag, isoil, cntyidx(cell)) = cRO2(mon, icrop, pflag, isoil, cntyidx(cell)) + RO2
            cETtrans(mon, icrop, pflag, isoil, cntyidx(cell)) = cETtrans(mon, icrop, pflag, isoil, cntyidx(cell)) + ETtrans
            cDP2RO(mon, icrop, pflag, isoil, cntyidx(cell)) = cDP2RO(mon, icrop, pflag, isoil, cntyidx(cell)) + DP2RO
            cSF(mon, icrop, pflag, isoil, cntyidx(cell)) = cSF(mon, icrop, pflag, isoil, cntyidx(cell)) + SF
            cRO2DP(mon, icrop, pflag, isoil, cntyidx(cell)) = cRO2DP(mon, icrop, pflag, isoil, cntyidx(cell)) + RO2DP
            cRO2ET(mon, icrop, pflag, isoil, cntyidx(cell)) = cRO2ET(mon, icrop, pflag, isoil, cntyidx(cell)) + RO2ET
            
            cfPrecip(mon, icrop, pflag, isoil, iczone) = cfPrecip(mon, icrop, pflag, isoil, iczone) + Precip(cell, mon) * acs / 12
            cfAcs(mon, icrop, pflag, isoil, iczone) = cfAcs(mon, icrop, pflag, isoil, iczone) + Acs
            cfAP(mon, icrop, pflag, isoil, iczone) = cfAP(mon, icrop, pflag, isoil, iczone) + AP
            cfSWD(mon, icrop, pflag, isoil, iczone) = cfSWD(mon, icrop, pflag, isoil, iczone) + SWD
            cfET(mon, icrop, pflag, isoil, iczone) = cfET(mon, icrop, pflag, isoil, iczone) + ET
            cfRO(mon, icrop, pflag, isoil, iczone) = cfRO(mon, icrop, pflag, isoil, iczone) + RO
            cfDP(mon, icrop, pflag, isoil, iczone) = cfDP(mon, icrop, pflag, isoil, iczone) + DP
            cfSL(mon, icrop, pflag, isoil, iczone) = cfSL(mon, icrop, pflag, isoil, iczone) + SL
            cfPSL(mon, icrop, pflag, isoil, iczone) = cfPSL(mon, icrop, pflag, isoil, iczone) + PSL
            cfDAP(mon, icrop, pflag, isoil, iczone) = cfDAP(mon, icrop, pflag, isoil, iczone) + DAP
            cfETG(mon, icrop, pflag, isoil, iczone) = cfETG(mon, icrop, pflag, isoil, iczone) + ETG
            cfDET(mon, icrop, pflag, isoil, iczone) = cfDET(mon, icrop, pflag, isoil, iczone) + DET
            cfETB(mon, icrop, pflag, isoil, iczone) = cfETB(mon, icrop, pflag, isoil, iczone) + ETB
            cfDP1(mon, icrop, pflag, isoil, iczone) = cfDP1(mon, icrop, pflag, isoil, iczone) + DP1
            cfDP2(mon, icrop, pflag, isoil, iczone) = cfDP2(mon, icrop, pflag, isoil, iczone) + DP2
            cfRO1(mon, icrop, pflag, isoil, iczone) = cfRO1(mon, icrop, pflag, isoil, iczone) + RO1
            cfRO2(mon, icrop, pflag, isoil, iczone) = cfRO2(mon, icrop, pflag, isoil, iczone) + RO2
            cfETtrans(mon, icrop, pflag, isoil, iczone) = cfETtrans(mon, icrop, pflag, isoil, iczone) + ETtrans
            cfDP2RO(mon, icrop, pflag, isoil, iczone) = cfDP2RO(mon, icrop, pflag, isoil, iczone) + DP2RO
            cfSF(mon, icrop, pflag, isoil, iczone) = cfSF(mon, icrop, pflag, isoil, iczone) + SF
            cfRO2DP(mon, icrop, pflag, isoil, iczone) = cfRO2DP(mon, icrop, pflag, isoil, iczone) + RO2DP
            cfRO2ET(mon, icrop, pflag, isoil, iczone) = cfRO2ET(mon, icrop, pflag, isoil, iczone) + RO2ET
            
            rzPrecip(mon, icrop, pflag, isoil, ROZ(cell)) = rzPrecip(mon, icrop, pflag, isoil, ROZ(cell)) + Precip(cell, mon) * acs / 12
            rzAcs(mon, icrop, pflag, isoil, ROZ(cell)) = rzAcs(mon, icrop, pflag, isoil, ROZ(cell)) + Acs
            rzAP(mon, icrop, pflag, isoil, ROZ(cell)) = rzAP(mon, icrop, pflag, isoil, ROZ(cell)) + AP
            rzSWD(mon, icrop, pflag, isoil, ROZ(cell)) = rzSWD(mon, icrop, pflag, isoil, ROZ(cell)) + SWD
            rzET(mon, icrop, pflag, isoil, ROZ(cell)) = rzET(mon, icrop, pflag, isoil, ROZ(cell)) + ET
            rzRO(mon, icrop, pflag, isoil, ROZ(cell)) = rzRO(mon, icrop, pflag, isoil, ROZ(cell)) + RO
            rzDP(mon, icrop, pflag, isoil, ROZ(cell)) = rzDP(mon, icrop, pflag, isoil, ROZ(cell)) + DP
            rzSL(mon, icrop, pflag, isoil, ROZ(cell)) = rzSL(mon, icrop, pflag, isoil, ROZ(cell)) + SL
            rzPSL(mon, icrop, pflag, isoil, ROZ(cell)) = rzPSL(mon, icrop, pflag, isoil, ROZ(cell)) + PSL
            rzDAP(mon, icrop, pflag, isoil, ROZ(cell)) = rzDAP(mon, icrop, pflag, isoil, ROZ(cell)) + DAP
            rzETG(mon, icrop, pflag, isoil, ROZ(cell)) = rzETG(mon, icrop, pflag, isoil, ROZ(cell)) + ETG
            rzDET(mon, icrop, pflag, isoil, ROZ(cell)) = rzDET(mon, icrop, pflag, isoil, ROZ(cell)) + DET
            rzETB(mon, icrop, pflag, isoil, ROZ(cell)) = rzETB(mon, icrop, pflag, isoil, ROZ(cell)) + ETB
            rzDP1(mon, icrop, pflag, isoil, ROZ(cell)) = rzDP1(mon, icrop, pflag, isoil, ROZ(cell)) + DP1
            rzDP2(mon, icrop, pflag, isoil, ROZ(cell)) = rzDP2(mon, icrop, pflag, isoil, ROZ(cell)) + DP2
            rzRO1(mon, icrop, pflag, isoil, ROZ(cell)) = rzRO1(mon, icrop, pflag, isoil, ROZ(cell)) + RO1
            rzRO2(mon, icrop, pflag, isoil, ROZ(cell)) = rzRO2(mon, icrop, pflag, isoil, ROZ(cell)) + RO2
            rzETtrans(mon, icrop, pflag, isoil, ROZ(cell)) = rzETtrans(mon, icrop, pflag, isoil, ROZ(cell)) + ETtrans
            rzDP2RO(mon, icrop, pflag, isoil, ROZ(cell)) = rzDP2RO(mon, icrop, pflag, isoil, ROZ(cell)) + DP2RO
            rzSF(mon, icrop, pflag, isoil, ROZ(cell)) = rzSF(mon, icrop, pflag, isoil, ROZ(cell)) + SF
            rzRO2DP(mon, icrop, pflag, isoil, ROZ(cell)) = rzRO2DP(mon, icrop, pflag, isoil, ROZ(cell)) + RO2DP
            rzRO2ET(mon, icrop, pflag, isoil, ROZ(cell)) = rzRO2ET(mon, icrop, pflag, isoil, ROZ(cell)) + RO2ET
            
        END DO
        
        CALL writereports

    END DO
    
    
    
END PROGRAM WSPP_Report
!************************************************************************
SUBROUTINE zerovals
    USE parReport
    IMPLICIT NONE
    
    rPrecip = 0.
    rAcs = 0.
    rAP = 0.
    rSWD = 0.
    rET = 0.
    rRO = 0.
    rDP = 0.
    rSL = 0.
    rPSL = 0.
    rDAP = 0.
    rETG = 0.
    rDET = 0.
    rETB = 0.
    rDP1 = 0.
    rDP2 = 0.
    rRO1 = 0.
    rRO2 = 0.
    rETtrans = 0.
    rDP2RO = 0.
    rSF = 0.
    rRO2DP = 0.
    rRO2ET = 0.
    
    cPrecip = 0.
    cAcs = 0.
    cAP = 0.
    cSWD = 0.
    cET = 0.
    cRO = 0.
    cDP = 0.
    cSL = 0.
    cPSL = 0.
    cDAP = 0.
    cETG = 0.
    cDET = 0.
    cETB = 0.
    cDP1 = 0.
    cDP2 = 0.
    cRO1 = 0.
    cRO2 = 0.
    cETtrans = 0.
    cDP2RO = 0.
    cSF = 0.
    cRO2DP = 0.
    cRO2ET = 0.
    
    cfPrecip = 0.
    cfAcs = 0.
    cfAP = 0.
    cfSWD = 0.
    cfET = 0.
    cfRO = 0.
    cfDP = 0.
    cfSL = 0.
    cfPSL = 0.
    cfDAP = 0.
    cfETG = 0.
    cfDET = 0.
    cfETB = 0.
    cfDP1 = 0.
    cfDP2 = 0.
    cfRO1 = 0.
    cfRO2 = 0.
    cfETtrans = 0.
    cfDP2RO = 0.
    cfSF = 0.
    cfRO2DP = 0.
    cfRO2ET = 0.
    
    rzPrecip = 0.
    rzAcs = 0.
    rzAP = 0.
    rzSWD = 0.
    rzET = 0.
    rzRO = 0.
    rzDP = 0.
    rzSL = 0.
    rzPSL = 0.
    rzDAP = 0.
    rzETG = 0.
    rzDET = 0.
    rzETB = 0.
    rzDP1 = 0.
    rzDP2 = 0.
    rzRO1 = 0.
    rzRO2 = 0.
    rzETtrans = 0.
    rzDP2RO = 0.
    rzSF = 0.
    rzRO2DP = 0.
    rzRO2ET = 0.
    
END SUBROUTINE zerovals
!********************************************************************
SUBROUTINE writereports
    USE parReport
    IMPLICIT NONE
    
    INTEGER     imon, irri, icounty
    
    REAL        wPrecip, wAcs, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP
    REAL        wETG, wDET, wETb, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO
    REAL        wSF, wRO2DP, wRO2ET

    !Regional Totals
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\Regional\Annual\Reg_Tot_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\Regional\Annual\Reg_Tot_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 100)
    WRITE (11, 100)
    
    DO imon = 0, 12
        wPrecip = SUM(rPrecip(imon, :, :, :))
        wAcs = SUM(rAcs(imon, :, :, :))
        wAP = SUM(rAP(imon, :, :, :))
        wSWD = SUM(rSWD(imon, :, :, :))
        wET = SUM(rET(imon, :, :, :))
        wRO = SUM(rRO(imon, :, :, :))
        wDP = SUM(rDP(imon, :, :, :))
        wSL = SUM(rSL(imon, :, :, :))
        wPSL = SUM(rPSL(imon, :, :, :))
        wDAP = SUM(rDAP(imon, :, :, :))
        wETG = SUM(rETG(imon, :, :, :))
        wDET = SUM(rDET(imon, :, :, :))
        wETB = SUM(rETB(imon, :, :, :))
        wDP1 = SUM(rDP1(imon, :, :, :))
        wDP2 = SUM(rDP2(imon, :, :, :))
        wRO1 = SUM(rRO1(imon, :, :, :))
        wRO2 = SUM(rRO2(imon, :, :, :))
        wETtrans = SUM(rETtrans(imon, :, :, :))
        wDP2RO = SUM(rDP2RO(imon, :, :, :))
        wSF = SUM(rSF(imon, :, :, :))
        wRO2DP = SUM(rRO2DP(imon, :, :, :))
        wRO2ET= SUM(rRO2ET(imon, :, :, :))
    
        IF (wAcs .GT. 0.) THEN
            IF (imon .EQ. 0) THEN
                WRITE (10, 200) iyear, imon, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
            ELSE
                WRITE (11, 200) iyear, imon, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
            END IF
        END IF
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Regional Crops
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\Regional\Annual\Reg_Crop_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\Regional\Annual\Reg_Crop_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 101)
    WRITE (11, 101)
    
    DO icrop = 1, ncrop
        DO imon = 0, 12
            wPrecip = SUM(rPrecip(imon, icrop, :, :))
            wAcs = SUM(rAcs(imon, icrop, :, :))
            wAP = SUM(rAP(imon, icrop, :, :))
            wSWD = SUM(rSWD(imon, icrop, :, :))
            wET = SUM(rET(imon, icrop, :, :))
            wRO = SUM(rRO(imon, icrop, :, :))
            wDP = SUM(rDP(imon, icrop, :, :))
            wSL = SUM(rSL(imon, icrop, :, :))
            wPSL = SUM(rPSL(imon, icrop, :, :))
            wDAP = SUM(rDAP(imon, icrop, :, :))
            wETG = SUM(rETG(imon, icrop, :, :))
            wDET = SUM(rDET(imon, icrop, :, :))
            wETB = SUM(rETB(imon, icrop, :, :))
            wDP1 = SUM(rDP1(imon, icrop, :, :))
            wDP2 = SUM(rDP2(imon, icrop, :, :))
            wRO1 = SUM(rRO1(imon, icrop, :, :))
            wRO2 = SUM(rRO2(imon, icrop, :, :))
            wETtrans = SUM(rETtrans(imon, icrop, :, :))
            wDP2RO = SUM(rDP2RO(imon, icrop, :, :))
            wSF = SUM(rSF(imon, icrop, :, :))
            wRO2DP = SUM(rRO2DP(imon, icrop, :, :))
            wRO2ET= SUM(rRO2ET(imon, icrop, :, :))
    
            IF (wAcs .GT. 0.) THEN
                IF (imon .EQ. 0) THEN
                    WRITE (10, 201) iyear, imon, icrop, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                    wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                ELSE
                    WRITE (11, 201) iyear, imon, icrop, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                    wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                END IF
            END IF
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Regional Soil
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\Regional\Annual\Reg_Soil_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\Regional\Annual\Reg_Soil_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 102)
    WRITE (11, 102)
    
    DO isoil = 1, nsoil
        DO imon = 0, 12
            wPrecip = SUM(rPrecip(imon, :, :, isoil))
            wAcs = SUM(rAcs(imon, :, :, isoil))
            wAP = SUM(rAP(imon, :, :, isoil))
            wSWD = SUM(rSWD(imon, :, :, isoil))
            wET = SUM(rET(imon, :, :, isoil))
            wRO = SUM(rRO(imon, :, :, isoil))
            wDP = SUM(rDP(imon, :, :, isoil))
            wSL = SUM(rSL(imon, :, :, isoil))
            wPSL = SUM(rPSL(imon, :, :, isoil))
            wDAP = SUM(rDAP(imon, :, :, isoil))
            wETG = SUM(rETG(imon, :, :, isoil))
            wDET = SUM(rDET(imon, :, :, isoil))
            wETB = SUM(rETB(imon, :, :, isoil))
            wDP1 = SUM(rDP1(imon, :, :, isoil))
            wDP2 = SUM(rDP2(imon, :, :, isoil))
            wRO1 = SUM(rRO1(imon, :, :, isoil))
            wRO2 = SUM(rRO2(imon, :, :, isoil))
            wETtrans = SUM(rETtrans(imon, :, :, isoil))
            wDP2RO = SUM(rDP2RO(imon, :, :, isoil))
            wSF = SUM(rSF(imon, :, :, isoil))
            wRO2DP = SUM(rRO2DP(imon, :, :, isoil))
            wRO2ET= SUM(rRO2ET(imon, :, :, isoil))
    
            IF (wAcs .GT. 0.) THEN
                IF (imon .EQ. 0) THEN
                    WRITE (10, 201) iyear, imon, isoil, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                    wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                ELSE
                    WRITE (11, 201) iyear, imon, isoil, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                    wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                END IF
            END IF
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Regional Irrigation Source
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\Regional\Annual\Reg_IrrS_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\Regional\Annual\Reg_IrrS_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 103)
    WRITE (11, 103)
    
    DO irri = 1, 4
        DO imon = 0, 12
            wPrecip = SUM(rPrecip(imon, :, irri, :))
            wAcs = SUM(rAcs(imon, :, irri, :))
            wAP = SUM(rAP(imon, :, irri, :))
            wSWD = SUM(rSWD(imon, :, irri, :))
            wET = SUM(rET(imon, :, irri, :))
            wRO = SUM(rRO(imon, :, irri, :))
            wDP = SUM(rDP(imon, :, irri, :))
            wSL = SUM(rSL(imon, :, irri, :))
            wPSL = SUM(rPSL(imon, :, irri, :))
            wDAP = SUM(rDAP(imon, :, irri, :))
            wETG = SUM(rETG(imon, :, irri, :))
            wDET = SUM(rDET(imon, :, irri, :))
            wETB = SUM(rETB(imon, :, irri, :))
            wDP1 = SUM(rDP1(imon, :, irri, :))
            wDP2 = SUM(rDP2(imon, :, irri, :))
            wRO1 = SUM(rRO1(imon, :, irri, :))
            wRO2 = SUM(rRO2(imon, :, irri, :))
            wETtrans = SUM(rETtrans(imon, :, irri, :))
            wDP2RO = SUM(rDP2RO(imon, :, irri, :))
            wSF = SUM(rSF(imon, :, irri, :))
            wRO2DP = SUM(rRO2DP(imon, :, irri, :))
            wRO2ET= SUM(rRO2ET(imon, :, irri, :))
    
            IF (wAcs .GT. 0.) THEN
                IF (imon .EQ. 0) THEN
                    WRITE (10, 201) iyear, imon, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                    wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                ELSE
                    WRITE (11, 201) iyear, imon, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                    wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                END IF
            END IF
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Regional Crops - Soil
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\Regional\Annual\Reg_CropSoil_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\Regional\Annual\Reg_CropSoil_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 104)
    WRITE (11, 104)
    
    DO icrop = 1, ncrop
        DO isoil = 1, nsoil
            DO imon = 0, 12
                wPrecip = SUM(rPrecip(imon, icrop, :, isoil))
                wAcs = SUM(rAcs(imon, icrop, :, isoil))
                wAP = SUM(rAP(imon, icrop, :, isoil))
                wSWD = SUM(rSWD(imon, icrop, :, isoil))
                wET = SUM(rET(imon, icrop, :, isoil))
                wRO = SUM(rRO(imon, icrop, :, isoil))
                wDP = SUM(rDP(imon, icrop, :, isoil))
                wSL = SUM(rSL(imon, icrop, :, isoil))
                wPSL = SUM(rPSL(imon, icrop, :, isoil))
                wDAP = SUM(rDAP(imon, icrop, :, isoil))
                wETG = SUM(rETG(imon, icrop, :, isoil))
                wDET = SUM(rDET(imon, icrop, :, isoil))
                wETB = SUM(rETB(imon, icrop, :, isoil))
                wDP1 = SUM(rDP1(imon, icrop, :, isoil))
                wDP2 = SUM(rDP2(imon, icrop, :, isoil))
                wRO1 = SUM(rRO1(imon, icrop, :, isoil))
                wRO2 = SUM(rRO2(imon, icrop, :, isoil))
                wETtrans = SUM(rETtrans(imon, icrop, :, isoil))
                wDP2RO = SUM(rDP2RO(imon, icrop, :, isoil))
                wSF = SUM(rSF(imon, icrop, :, isoil))
                wRO2DP = SUM(rRO2DP(imon, icrop, :, isoil))
                wRO2ET= SUM(rRO2ET(imon, icrop, :, isoil))
    
                IF (wAcs .GT. 0.) THEN
                    IF (imon .EQ. 0) THEN
                        WRITE (10, 202) iyear, imon, icrop, isoil, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                        wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                    ELSE
                        WRITE (11, 202) iyear, imon, icrop, isoil, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                        wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                    END IF
                END IF
            END DO
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Regional Crops - Irrigation Source
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\Regional\Annual\Reg_CropIrrS_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\Regional\Annual\Reg_CropIrrS_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 105)
    WRITE (11, 105)
    
    DO icrop = 1, ncrop
        DO irri = 1, 4
            DO imon = 0, 12
                wPrecip = SUM(rPrecip(imon, icrop, irri, :))
                wAcs = SUM(rAcs(imon, icrop, irri, :))
                wAP = SUM(rAP(imon, icrop, irri, :))
                wSWD = SUM(rSWD(imon, icrop, irri, :))
                wET = SUM(rET(imon, icrop, irri, :))
                wRO = SUM(rRO(imon, icrop, irri, :))
                wDP = SUM(rDP(imon, icrop, irri, :))
                wSL = SUM(rSL(imon, icrop, irri, :))
                wPSL = SUM(rPSL(imon, icrop, irri, :))
                wDAP = SUM(rDAP(imon, icrop, irri, :))
                wETG = SUM(rETG(imon, icrop, irri, :))
                wDET = SUM(rDET(imon, icrop, irri, :))
                wETB = SUM(rETB(imon, icrop, irri, :))
                wDP1 = SUM(rDP1(imon, icrop, irri, :))
                wDP2 = SUM(rDP2(imon, icrop, irri, :))
                wRO1 = SUM(rRO1(imon, icrop, irri, :))
                wRO2 = SUM(rRO2(imon, icrop, irri, :))
                wETtrans = SUM(rETtrans(imon, icrop, irri, :))
                wDP2RO = SUM(rDP2RO(imon, icrop, irri, :))
                wSF = SUM(rSF(imon, icrop, irri, :))
                wRO2DP = SUM(rRO2DP(imon, icrop, irri, :))
                wRO2ET= SUM(rRO2ET(imon, icrop, irri, :))
    
                IF (wAcs .GT. 0.) THEN
                    IF (imon .EQ. 0) THEN
                        WRITE (10, 202) iyear, imon, icrop, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                        wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                    ELSE
                        WRITE (11, 202) iyear, imon, icrop, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                        wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                    END IF
                END IF
            END DO
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Regional Soil - Irrigation Source
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\Regional\Annual\Reg_SoilIrrS_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\Regional\Annual\Reg_SoilIrrS_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 106)
    WRITE (11, 106)
    
    DO isoil = 1, nsoil
        DO irri = 1, 4
            DO imon = 0, 12
                wPrecip = SUM(rPrecip(imon, :, irri, isoil))
                wAcs = SUM(rAcs(imon, :, irri, isoil))
                wAP = SUM(rAP(imon, :, irri, isoil))
                wSWD = SUM(rSWD(imon, :, irri, isoil))
                wET = SUM(rET(imon, :, irri, isoil))
                wRO = SUM(rRO(imon, :, irri, isoil))
                wDP = SUM(rDP(imon, :, irri, isoil))
                wSL = SUM(rSL(imon, :, irri, isoil))
                wPSL = SUM(rPSL(imon, :, irri, isoil))
                wDAP = SUM(rDAP(imon, :, irri, isoil))
                wETG = SUM(rETG(imon, :, irri, isoil))
                wDET = SUM(rDET(imon, :, irri, isoil))
                wETB = SUM(rETB(imon, :, irri, isoil))
                wDP1 = SUM(rDP1(imon, :, irri, isoil))
                wDP2 = SUM(rDP2(imon, :, irri, isoil))
                wRO1 = SUM(rRO1(imon, :, irri, isoil))
                wRO2 = SUM(rRO2(imon, :, irri, isoil))
                wETtrans = SUM(rETtrans(imon, :, irri, isoil))
                wDP2RO = SUM(rDP2RO(imon, :, irri, isoil))
                wSF = SUM(rSF(imon, :, irri, isoil))
                wRO2DP = SUM(rRO2DP(imon, :, irri, isoil))
                wRO2ET= SUM(rRO2ET(imon, :, irri, isoil))
    
                IF (wAcs .GT. 0.) THEN
                    IF (imon .EQ. 0) THEN
                        WRITE (10, 202) iyear, imon, isoil, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                        wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                    ELSE
                        WRITE (11, 202) iyear, imon, isoil, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                        wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                    END IF
                END IF
            END DO
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Regional Crop - Soil - Irrigation Source
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\Regional\Annual\Reg_CSI_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\Regional\Annual\Reg_CSI_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 107)
    WRITE (11, 107)
    
    DO icrop = 1, ncrop
        DO isoil = 1, nsoil
            DO irri = 1, 4
                DO imon = 0, 12
                    wPrecip = rPrecip(imon, icrop, irri, isoil)
                    wAcs = rAcs(imon, icrop, irri, isoil)
                    wAP = rAP(imon, icrop, irri, isoil)
                    wSWD = rSWD(imon, icrop, irri, isoil)
                    wET = rET(imon, icrop, irri, isoil)
                    wRO = rRO(imon, icrop, irri, isoil)
                    wDP = rDP(imon, icrop, irri, isoil)
                    wSL = rSL(imon, icrop, irri, isoil)
                    wPSL = rPSL(imon, icrop, irri, isoil)
                    wDAP = rDAP(imon, icrop, irri, isoil)
                    wETG = rETG(imon, icrop, irri, isoil)
                    wDET = rDET(imon, icrop, irri, isoil)
                    wETB = rETB(imon, icrop, irri, isoil)
                    wDP1 = rDP1(imon, icrop, irri, isoil)
                    wDP2 = rDP2(imon, icrop, irri, isoil)
                    wRO1 = rRO1(imon, icrop, irri, isoil)
                    wRO2 = rRO2(imon, icrop, irri, isoil)
                    wETtrans = rETtrans(imon, icrop, irri, isoil)
                    wDP2RO = rDP2RO(imon, icrop, irri, isoil)
                    wSF = rSF(imon, icrop, irri, isoil)
                    wRO2DP = rRO2DP(imon, icrop, irri, isoil)
                    wRO2ET= rRO2ET(imon, icrop, irri, isoil)
    
                    IF (wAcs .GT. 0.) THEN
                        IF (imon .EQ. 0) THEN
                            WRITE (10, 203) iyear, imon, icrop, isoil, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                            wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                        ELSE
                            WRITE (11, 203) iyear, imon, icrop, isoil, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                            wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                        END IF
                    END IF
                END DO
            END DO
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !County Totals
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\County\Annual\Cnty_Tot_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\County\Annual\Cnty_Tot_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 110)
    WRITE (11, 110)
    
    DO icounty = 1, ncounty
        IF (Actcounty(icounty) .EQ. 1) THEN
            DO imon = 0, 12
                wPrecip = SUM(cPrecip(imon, :, :, :, icounty))
                wAcs = SUM(cAcs(imon, :, :, :, icounty))
                wAP = SUM(cAP(imon, :, :, :, icounty))
                wSWD = SUM(cSWD(imon, :, :, :, icounty))
                wET = SUM(cET(imon, :, :, :, icounty))
                wRO = SUM(cRO(imon, :, :, :, icounty))
                wDP = SUM(cDP(imon, :, :, :, icounty))
                wSL = SUM(cSL(imon, :, :, :, icounty))
                wPSL = SUM(cPSL(imon, :, :, :, icounty))
                wDAP = SUM(cDAP(imon, :, :, :, icounty))
                wETG = SUM(cETG(imon, :, :, :, icounty))
                wDET = SUM(cDET(imon, :, :, :, icounty))
                wETB = SUM(cETB(imon, :, :, :, icounty))
                wDP1 = SUM(cDP1(imon, :, :, :, icounty))
                wDP2 = SUM(cDP2(imon, :, :, :, icounty))
                wRO1 = SUM(cRO1(imon, :, :, :, icounty))
                wRO2 = SUM(cRO2(imon, :, :, :, icounty))
                wETtrans = SUM(cETtrans(imon, :, :, :, icounty))
                wDP2RO = SUM(cDP2RO(imon, :, :, :, icounty))
                wSF = SUM(cSF(imon, :, :, :, icounty))
                wRO2DP = SUM(cRO2DP(imon, :, :, :, icounty))
                wRO2ET= SUM(cRO2ET(imon, :, :, :, icounty))
    
                IF (wAcs .GT. 0.) THEN
                    IF (imon .EQ. 0) THEN
                        WRITE (10, 210) iyear, imon, icounty, CountyName(icounty), cstate(icounty), wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                        wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                    ELSE
                        WRITE (11, 210) iyear, imon, icounty, CountyName(icounty), cstate(icounty), wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                        wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                    END IF
                END IF
            END DO
        END IF
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !County Crops
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\County\Annual\Cnty_Crop_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\County\Annual\Cnty_Crop_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 111)
    WRITE (11, 111)
    
    DO icounty = 1, ncounty
        IF (Actcounty(icounty) .EQ. 1) THEN
            DO icrop = 1, ncrop
                DO imon = 0, 12
                    wPrecip = SUM(cPrecip(imon, icrop, :, :, icounty))
                    wAcs = SUM(cAcs(imon, icrop, :, :, icounty))
                    wAP = SUM(cAP(imon, icrop, :, :, icounty))
                    wSWD = SUM(cSWD(imon, icrop, :, :, icounty))
                    wET = SUM(cET(imon, icrop, :, :, icounty))
                    wRO = SUM(cRO(imon, icrop, :, :, icounty))
                    wDP = SUM(cDP(imon, icrop, :, :, icounty))
                    wSL = SUM(cSL(imon, icrop, :, :, icounty))
                    wPSL = SUM(cPSL(imon, icrop, :, :, icounty))
                    wDAP = SUM(cDAP(imon, icrop, :, :, icounty))
                    wETG = SUM(cETG(imon, icrop, :, :, icounty))
                    wDET = SUM(cDET(imon, icrop, :, :, icounty))
                    wETB = SUM(cETB(imon, icrop, :, :, icounty))
                    wDP1 = SUM(cDP1(imon, icrop, :, :, icounty))
                    wDP2 = SUM(cDP2(imon, icrop, :, :, icounty))
                    wRO1 = SUM(cRO1(imon, icrop, :, :, icounty))
                    wRO2 = SUM(cRO2(imon, icrop, :, :, icounty))
                    wETtrans = SUM(cETtrans(imon, icrop, :, :, icounty))
                    wDP2RO = SUM(cDP2RO(imon, icrop, :, :, icounty))
                    wSF = SUM(cSF(imon, icrop, :, :, icounty))
                    wRO2DP = SUM(cRO2DP(imon, icrop, :, :, icounty))
                    wRO2ET= SUM(cRO2ET(imon, icrop, :, :, icounty))
        
                    IF (wAcs .GT. 0.) THEN
                        IF (imon .EQ. 0) THEN
                            WRITE (10, 211) iyear, imon, icounty, CountyName(icounty), cstate(icounty), icrop, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                            wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                        ELSE
                            WRITE (11, 211) iyear, imon, icounty, CountyName(icounty), cstate(icounty), icrop, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                            wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                        END IF
                    END IF
                END DO
            END DO
        END IF
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !County Soils
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\County\Annual\Cnty_Soil_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\County\Annual\Cnty_Soil_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 112)
    WRITE (11, 112)
    
    DO icounty = 1, ncounty
        IF (Actcounty(icounty) .EQ. 1) THEN
            DO isoil = 1, nsoil
                DO imon = 0, 12
                    wPrecip = SUM(cPrecip(imon, :, :, isoil, icounty))
                    wAcs = SUM(cAcs(imon, :, :, isoil, icounty))
                    wAP = SUM(cAP(imon, :, :, isoil, icounty))
                    wSWD = SUM(cSWD(imon, :, :, isoil, icounty))
                    wET = SUM(cET(imon, :, :, isoil, icounty))
                    wRO = SUM(cRO(imon, :, :, isoil, icounty))
                    wDP = SUM(cDP(imon, :, :, isoil, icounty))
                    wSL = SUM(cSL(imon, :, :, isoil, icounty))
                    wPSL = SUM(cPSL(imon, :, :, isoil, icounty))
                    wDAP = SUM(cDAP(imon, :, :, isoil, icounty))
                    wETG = SUM(cETG(imon, :, :, isoil, icounty))
                    wDET = SUM(cDET(imon, :, :, isoil, icounty))
                    wETB = SUM(cETB(imon, :, :, isoil, icounty))
                    wDP1 = SUM(cDP1(imon, :, :, isoil, icounty))
                    wDP2 = SUM(cDP2(imon, :, :, isoil, icounty))
                    wRO1 = SUM(cRO1(imon, :, :, isoil, icounty))
                    wRO2 = SUM(cRO2(imon, :, :, isoil, icounty))
                    wETtrans = SUM(cETtrans(imon, :, :, isoil, icounty))
                    wDP2RO = SUM(cDP2RO(imon, :, :, isoil, icounty))
                    wSF = SUM(cSF(imon, :, :, isoil, icounty))
                    wRO2DP = SUM(cRO2DP(imon, :, :, isoil, icounty))
                    wRO2ET= SUM(cRO2ET(imon, :, :, isoil, icounty))
        
                    IF (wAcs .GT. 0.) THEN
                        IF (imon .EQ. 0) THEN
                            WRITE (10, 211) iyear, imon, icounty, CountyName(icounty), cstate(icounty), isoil, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                            wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                        ELSE
                            WRITE (11, 211) iyear, imon, icounty, CountyName(icounty), cstate(icounty), isoil, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                            wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                        END IF
                    END IF
                END DO
            END DO
        END IF
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !County Irrigation Source
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\County\Annual\Cnty_IrrS_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\County\Annual\Cnty_IrrS_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 113)
    WRITE (11, 113)
    
    DO icounty = 1, ncounty
        IF (Actcounty(icounty) .EQ. 1) THEN
            DO irri = 1, 4
                DO imon = 0, 12
                    wPrecip = SUM(cPrecip(imon, :, irri, :, icounty))
                    wAcs = SUM(cAcs(imon, :, irri, :, icounty))
                    wAP = SUM(cAP(imon, :, irri, :, icounty))
                    wSWD = SUM(cSWD(imon, :, irri, :, icounty))
                    wET = SUM(cET(imon, :, irri, :, icounty))
                    wRO = SUM(cRO(imon, :, irri, :, icounty))
                    wDP = SUM(cDP(imon, :, irri, :, icounty))
                    wSL = SUM(cSL(imon, :, irri, :, icounty))
                    wPSL = SUM(cPSL(imon, :, irri, :, icounty))
                    wDAP = SUM(cDAP(imon, :, irri, :, icounty))
                    wETG = SUM(cETG(imon, :, irri, :, icounty))
                    wDET = SUM(cDET(imon, :, irri, :, icounty))
                    wETB = SUM(cETB(imon, :, irri, :, icounty))
                    wDP1 = SUM(cDP1(imon, :, irri, :, icounty))
                    wDP2 = SUM(cDP2(imon, :, irri, :, icounty))
                    wRO1 = SUM(cRO1(imon, :, irri, :, icounty))
                    wRO2 = SUM(cRO2(imon, :, irri, :, icounty))
                    wETtrans = SUM(cETtrans(imon, :, irri, :, icounty))
                    wDP2RO = SUM(cDP2RO(imon, :, irri, :, icounty))
                    wSF = SUM(cSF(imon, :, irri, :, icounty))
                    wRO2DP = SUM(cRO2DP(imon, :, irri, :, icounty))
                    wRO2ET= SUM(cRO2ET(imon, :, irri, :, icounty))
        
                    IF (wAcs .GT. 0.) THEN
                        IF (imon .EQ. 0) THEN
                            WRITE (10, 211) iyear, imon, icounty, CountyName(icounty), cstate(icounty), irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                            wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                        ELSE
                            WRITE (11, 211) iyear, imon, icounty, CountyName(icounty), cstate(icounty), irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                            wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                        END IF
                    END IF
                END DO
            END DO
        END IF
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !County Crop - Soil
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\County\Annual\Cnty_CropSoil_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\County\Annual\Cnty_CropSoil_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 114)
    WRITE (11, 114)
    
    DO icounty = 1, ncounty
        IF (Actcounty(icounty) .EQ. 1) THEN
            DO icrop = 1, ncrop
                DO isoil = 1, nsoil
                    DO imon = 0, 12
                        wPrecip = SUM(cPrecip(imon, icrop, :, isoil, icounty))
                        wAcs = SUM(cAcs(imon, icrop, :, isoil, icounty))
                        wAP = SUM(cAP(imon, icrop, :, isoil, icounty))
                        wSWD = SUM(cSWD(imon, icrop, :, isoil, icounty))
                        wET = SUM(cET(imon, icrop, :, isoil, icounty))
                        wRO = SUM(cRO(imon, icrop, :, isoil, icounty))
                        wDP = SUM(cDP(imon, icrop, :, isoil, icounty))
                        wSL = SUM(cSL(imon, icrop, :, isoil, icounty))
                        wPSL = SUM(cPSL(imon, icrop, :, isoil, icounty))
                        wDAP = SUM(cDAP(imon, icrop, :, isoil, icounty))
                        wETG = SUM(cETG(imon, icrop, :, isoil, icounty))
                        wDET = SUM(cDET(imon, icrop, :, isoil, icounty))
                        wETB = SUM(cETB(imon, icrop, :, isoil, icounty))
                        wDP1 = SUM(cDP1(imon, icrop, :, isoil, icounty))
                        wDP2 = SUM(cDP2(imon, icrop, :, isoil, icounty))
                        wRO1 = SUM(cRO1(imon, icrop, :, isoil, icounty))
                        wRO2 = SUM(cRO2(imon, icrop, :, isoil, icounty))
                        wETtrans = SUM(cETtrans(imon, icrop, :, isoil, icounty))
                        wDP2RO = SUM(cDP2RO(imon, icrop, :, isoil, icounty))
                        wSF = SUM(cSF(imon, icrop, :, isoil, icounty))
                        wRO2DP = SUM(cRO2DP(imon, icrop, :, isoil, icounty))
                        wRO2ET= SUM(cRO2ET(imon, icrop, :, isoil, icounty))
        
                        IF (wAcs .GT. 0.) THEN
                            IF (imon .EQ. 0) THEN
                                WRITE (10, 212) iyear, imon, icounty, CountyName(icounty), cstate(icounty), icrop, isoil, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                                wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                            ELSE
                                WRITE (11, 212) iyear, imon, icounty, CountyName(icounty), cstate(icounty), icrop, isoil, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                                wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                            END IF
                        END IF
                    END DO
                END DO
            END DO
        END IF
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !County Crop - Irrigation Source
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\County\Annual\Cnty_CropIrrS_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\County\Annual\Cnty_CropIrrS_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 115)
    WRITE (11, 115)
    
    DO icounty = 1, ncounty
        IF (Actcounty(icounty) .EQ. 1) THEN
            DO icrop = 1, ncrop
                DO irri = 1, 4
                    DO imon = 0, 12
                        wPrecip = SUM(cPrecip(imon, icrop, irri, :, icounty))
                        wAcs = SUM(cAcs(imon, icrop, irri, :, icounty))
                        wAP = SUM(cAP(imon, icrop, irri, :, icounty))
                        wSWD = SUM(cSWD(imon, icrop, irri, :, icounty))
                        wET = SUM(cET(imon, icrop, irri, :, icounty))
                        wRO = SUM(cRO(imon, icrop, irri, :, icounty))
                        wDP = SUM(cDP(imon, icrop, irri, :, icounty))
                        wSL = SUM(cSL(imon, icrop, irri, :, icounty))
                        wPSL = SUM(cPSL(imon, icrop, irri, :, icounty))
                        wDAP = SUM(cDAP(imon, icrop, irri, :, icounty))
                        wETG = SUM(cETG(imon, icrop, irri, :, icounty))
                        wDET = SUM(cDET(imon, icrop, irri, :, icounty))
                        wETB = SUM(cETB(imon, icrop, irri, :, icounty))
                        wDP1 = SUM(cDP1(imon, icrop, irri, :, icounty))
                        wDP2 = SUM(cDP2(imon, icrop, irri, :, icounty))
                        wRO1 = SUM(cRO1(imon, icrop, irri, :, icounty))
                        wRO2 = SUM(cRO2(imon, icrop, irri, :, icounty))
                        wETtrans = SUM(cETtrans(imon, icrop, irri, :, icounty))
                        wDP2RO = SUM(cDP2RO(imon, icrop, irri, :, icounty))
                        wSF = SUM(cSF(imon, icrop, irri, :, icounty))
                        wRO2DP = SUM(cRO2DP(imon, icrop, irri, :, icounty))
                        wRO2ET= SUM(cRO2ET(imon, icrop, irri, :, icounty))
        
                        IF (wAcs .GT. 0.) THEN
                            IF (imon .EQ. 0) THEN
                                WRITE (10, 212) iyear, imon, icounty, CountyName(icounty), cstate(icounty), icrop, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                                wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                            ELSE
                                WRITE (11, 212) iyear, imon, icounty, CountyName(icounty), cstate(icounty), icrop, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                                wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                            END IF
                        END IF
                    END DO
                END DO
            END DO
        END IF
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !County Soil - Irrigation Source
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\County\Annual\Cnty_SoilIrrS_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\County\Annual\Cnty_SoilIrrS_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 116)
    WRITE (11, 116)
    
    DO icounty = 1, ncounty
        IF (Actcounty(icounty) .EQ. 1) THEN
            DO isoil = 1, nsoil
                DO irri = 1, 4
                    DO imon = 0, 12
                        wPrecip = SUM(cPrecip(imon, :, irri, isoil, icounty))
                        wAcs = SUM(cAcs(imon, :, irri, isoil, icounty))
                        wAP = SUM(cAP(imon, :, irri, isoil, icounty))
                        wSWD = SUM(cSWD(imon, :, irri, isoil, icounty))
                        wET = SUM(cET(imon, :, irri, isoil, icounty))
                        wRO = SUM(cRO(imon, :, irri, isoil, icounty))
                        wDP = SUM(cDP(imon, :, irri, isoil, icounty))
                        wSL = SUM(cSL(imon, :, irri, isoil, icounty))
                        wPSL = SUM(cPSL(imon, :, irri, isoil, icounty))
                        wDAP = SUM(cDAP(imon, :, irri, isoil, icounty))
                        wETG = SUM(cETG(imon, :, irri, isoil, icounty))
                        wDET = SUM(cDET(imon, :, irri, isoil, icounty))
                        wETB = SUM(cETB(imon, :, irri, isoil, icounty))
                        wDP1 = SUM(cDP1(imon, :, irri, isoil, icounty))
                        wDP2 = SUM(cDP2(imon, :, irri, isoil, icounty))
                        wRO1 = SUM(cRO1(imon, :, irri, isoil, icounty))
                        wRO2 = SUM(cRO2(imon, :, irri, isoil, icounty))
                        wETtrans = SUM(cETtrans(imon, :, irri, isoil, icounty))
                        wDP2RO = SUM(cDP2RO(imon, :, irri, isoil, icounty))
                        wSF = SUM(cSF(imon, :, irri, isoil, icounty))
                        wRO2DP = SUM(cRO2DP(imon, :, irri, isoil, icounty))
                        wRO2ET= SUM(cRO2ET(imon, :, irri, isoil, icounty))
        
                        IF (wAcs .GT. 0.) THEN
                            IF (imon .EQ. 0) THEN
                                WRITE (10, 212) iyear, imon, icounty, CountyName(icounty), cstate(icounty), isoil, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                                wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                            ELSE
                                WRITE (11, 212) iyear, imon, icounty, CountyName(icounty), cstate(icounty), isoil, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                                wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                            END IF
                        END IF
                    END DO
                END DO
            END DO
        END IF
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !County Crop - Soil - Irrigation Source
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\County\Annual\Cnty_CSI_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\County\Annual\Cnty_CSI_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 117)
    WRITE (11, 117)
    
    DO icounty = 1, ncounty
        IF (Actcounty(icounty) .EQ. 1) THEN
            DO icrop = 1, ncrop
                DO isoil = 1, nsoil
                    DO irri = 1, 4
                        DO imon = 0, 12
                            wPrecip = cPrecip(imon, icrop, irri, isoil, icounty)
                            wAcs = cAcs(imon, icrop, irri, isoil, icounty)
                            wAP = cAP(imon, icrop, irri, isoil, icounty)
                            wSWD = cSWD(imon, icrop, irri, isoil, icounty)
                            wET = cET(imon, icrop, irri, isoil, icounty)
                            wRO = cRO(imon, icrop, irri, isoil, icounty)
                            wDP = cDP(imon, icrop, irri, isoil, icounty)
                            wSL = cSL(imon, icrop, irri, isoil, icounty)
                            wPSL = cPSL(imon, icrop, irri, isoil, icounty)
                            wDAP = cDAP(imon, icrop, irri, isoil, icounty)
                            wETG = cETG(imon, icrop, irri, isoil, icounty)
                            wDET = cDET(imon, icrop, irri, isoil, icounty)
                            wETB = cETB(imon, icrop, irri, isoil, icounty)
                            wDP1 = cDP1(imon, icrop, irri, isoil, icounty)
                            wDP2 = cDP2(imon, icrop, irri, isoil, icounty)
                            wRO1 = cRO1(imon, icrop, irri, isoil, icounty)
                            wRO2 = cRO2(imon, icrop, irri, isoil, icounty)
                            wETtrans = cETtrans(imon, icrop, irri, isoil, icounty)
                            wDP2RO = cDP2RO(imon, icrop, irri, isoil, icounty)
                            wSF = cSF(imon, icrop, irri, isoil, icounty)
                            wRO2DP = cRO2DP(imon, icrop, irri, isoil, icounty)
                            wRO2ET= cRO2ET(imon, icrop, irri, isoil, icounty)
        
                            IF (wAcs .GT. 0.) THEN
                                IF (imon .EQ. 0) THEN
                                    WRITE (10, 213) iyear, imon, icounty, CountyName(icounty), cstate(icounty), icrop, isoil, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                                    wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                                ELSE
                                    WRITE (11, 213) iyear, imon, icounty, CountyName(icounty), cstate(icounty), icrop, isoil, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                                    wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                                END IF
                            END IF
                        END DO
                    END DO
                END DO
            END DO
        END IF
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Coefficient Zone Totals
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\CoefZone\Annual\CoefZ_Tot_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\CoefZone\Annual\CoefZ_Tot_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 120)
    WRITE (11, 120)
    
    DO iczone = 1, nzones
        DO imon = 0, 12
            wPrecip = SUM(cfPrecip(imon, :, :, :, iczone))
            wAcs = SUM(cfAcs(imon, :, :, :, iczone))
            wAP = SUM(cfAP(imon, :, :, :, iczone))
            wSWD = SUM(cfSWD(imon, :, :, :, iczone))
            wET = SUM(cfET(imon, :, :, :, iczone))
            wRO = SUM(cfRO(imon, :, :, :, iczone))
            wDP = SUM(cfDP(imon, :, :, :, iczone))
            wSL = SUM(cfSL(imon, :, :, :, iczone))
            wPSL = SUM(cfPSL(imon, :, :, :, iczone))
            wDAP = SUM(cfDAP(imon, :, :, :, iczone))
            wETG = SUM(cfETG(imon, :, :, :, iczone))
            wDET = SUM(cfDET(imon, :, :, :, iczone))
            wETB = SUM(cfETB(imon, :, :, :, iczone))
            wDP1 = SUM(cfDP1(imon, :, :, :, iczone))
            wDP2 = SUM(cfDP2(imon, :, :, :, iczone))
            wRO1 = SUM(cfRO1(imon, :, :, :, iczone))
            wRO2 = SUM(cfRO2(imon, :, :, :, iczone))
            wETtrans = SUM(cfETtrans(imon, :, :, :, iczone))
            wDP2RO = SUM(cfDP2RO(imon, :, :, :, iczone))
            wSF = SUM(cfSF(imon, :, :, :, iczone))
            wRO2DP = SUM(cfRO2DP(imon, :, :, :, iczone))
            wRO2ET= SUM(cfRO2ET(imon, :, :, :, iczone))
    
            IF (wAcs .GT. 0.) THEN
                IF (imon .EQ. 0) THEN
                    WRITE (10, 201) iyear, imon, iczone, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                    wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                ELSE
                    WRITE (11, 201) iyear, imon, iczone, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                    wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                END IF
            END IF
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Coefficient Zone Crop
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\CoefZone\Annual\CoefZ_Crop_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\CoefZone\Annual\CoefZ_Crop_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 121)
    WRITE (11, 121)
    
    DO iczone = 1, nzones
        DO icrop = 1, ncrop
            DO imon = 0, 12
                wPrecip = SUM(cfPrecip(imon, icrop, :, :, iczone))
                wAcs = SUM(cfAcs(imon, icrop, :, :, iczone))
                wAP = SUM(cfAP(imon, icrop, :, :, iczone))
                wSWD = SUM(cfSWD(imon, icrop, :, :, iczone))
                wET = SUM(cfET(imon, icrop, :, :, iczone))
                wRO = SUM(cfRO(imon, icrop, :, :, iczone))
                wDP = SUM(cfDP(imon, icrop, :, :, iczone))
                wSL = SUM(cfSL(imon, icrop, :, :, iczone))
                wPSL = SUM(cfPSL(imon, icrop, :, :, iczone))
                wDAP = SUM(cfDAP(imon, icrop, :, :, iczone))
                wETG = SUM(cfETG(imon, icrop, :, :, iczone))
                wDET = SUM(cfDET(imon, icrop, :, :, iczone))
                wETB = SUM(cfETB(imon, icrop, :, :, iczone))
                wDP1 = SUM(cfDP1(imon, icrop, :, :, iczone))
                wDP2 = SUM(cfDP2(imon, icrop, :, :, iczone))
                wRO1 = SUM(cfRO1(imon, icrop, :, :, iczone))
                wRO2 = SUM(cfRO2(imon, icrop, :, :, iczone))
                wETtrans = SUM(cfETtrans(imon, icrop, :, :, iczone))
                wDP2RO = SUM(cfDP2RO(imon, icrop, :, :, iczone))
                wSF = SUM(cfSF(imon, icrop, :, :, iczone))
                wRO2DP = SUM(cfRO2DP(imon, icrop, :, :, iczone))
                wRO2ET= SUM(cfRO2ET(imon, icrop, :, :, iczone))
    
                IF (wAcs .GT. 0.) THEN
                    IF (imon .EQ. 0) THEN
                        WRITE (10, 202) iyear, imon, iczone, icrop, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                        wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                    ELSE
                        WRITE (11, 202) iyear, imon, iczone, icrop, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                        wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                    END IF
                END IF
            END DO
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Coefficient Zone Soil
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\CoefZone\Annual\CoefZ_Soil_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\CoefZone\Annual\CoefZ_Soil_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 122)
    WRITE (11, 122)
    
    DO iczone = 1, nzones
        DO isoil = 1, nsoil
            DO imon = 0, 12
                wPrecip = SUM(cfPrecip(imon, :, :, isoil, iczone))
                wAcs = SUM(cfAcs(imon, :, :, isoil, iczone))
                wAP = SUM(cfAP(imon, :, :, isoil, iczone))
                wSWD = SUM(cfSWD(imon, :, :, isoil, iczone))
                wET = SUM(cfET(imon, :, :, isoil, iczone))
                wRO = SUM(cfRO(imon, :, :, isoil, iczone))
                wDP = SUM(cfDP(imon, :, :, isoil, iczone))
                wSL = SUM(cfSL(imon, :, :, isoil, iczone))
                wPSL = SUM(cfPSL(imon, :, :, isoil, iczone))
                wDAP = SUM(cfDAP(imon, :, :, isoil, iczone))
                wETG = SUM(cfETG(imon, :, :, isoil, iczone))
                wDET = SUM(cfDET(imon, :, :, isoil, iczone))
                wETB = SUM(cfETB(imon, :, :, isoil, iczone))
                wDP1 = SUM(cfDP1(imon, :, :, isoil, iczone))
                wDP2 = SUM(cfDP2(imon, :, :, isoil, iczone))
                wRO1 = SUM(cfRO1(imon, :, :, isoil, iczone))
                wRO2 = SUM(cfRO2(imon, :, :, isoil, iczone))
                wETtrans = SUM(cfETtrans(imon, :, :, isoil, iczone))
                wDP2RO = SUM(cfDP2RO(imon, :, :, isoil, iczone))
                wSF = SUM(cfSF(imon, :, :, isoil, iczone))
                wRO2DP = SUM(cfRO2DP(imon, :, :, isoil, iczone))
                wRO2ET= SUM(cfRO2ET(imon, :, :, isoil, iczone))
    
                IF (wAcs .GT. 0.) THEN
                    IF (imon .EQ. 0) THEN
                        WRITE (10, 202) iyear, imon, iczone, isoil, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                        wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                    ELSE
                        WRITE (11, 202) iyear, imon, iczone, isoil, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                        wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                    END IF
                END IF
            END DO
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Coefficient Zone Irrigation Source
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\CoefZone\Annual\CoefZ_IrrS_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\CoefZone\Annual\CoefZ_IrrS_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 123)
    WRITE (11, 123)
    
    DO iczone = 1, nzones
        DO irri = 1, 4
            DO imon = 0, 12
                wPrecip = SUM(cfPrecip(imon, :, irri, :, iczone))
                wAcs = SUM(cfAcs(imon, :, irri, :, iczone))
                wAP = SUM(cfAP(imon, :, irri, :, iczone))
                wSWD = SUM(cfSWD(imon, :, irri, :, iczone))
                wET = SUM(cfET(imon, :, irri, :, iczone))
                wRO = SUM(cfRO(imon, :, irri, :, iczone))
                wDP = SUM(cfDP(imon, :, irri, :, iczone))
                wSL = SUM(cfSL(imon, :, irri, :, iczone))
                wPSL = SUM(cfPSL(imon, :, irri, :, iczone))
                wDAP = SUM(cfDAP(imon, :, irri, :, iczone))
                wETG = SUM(cfETG(imon, :, irri, :, iczone))
                wDET = SUM(cfDET(imon, :, irri, :, iczone))
                wETB = SUM(cfETB(imon, :, irri, :, iczone))
                wDP1 = SUM(cfDP1(imon, :, irri, :, iczone))
                wDP2 = SUM(cfDP2(imon, :, irri, :, iczone))
                wRO1 = SUM(cfRO1(imon, :, irri, :, iczone))
                wRO2 = SUM(cfRO2(imon, :, irri, :, iczone))
                wETtrans = SUM(cfETtrans(imon, :, irri, :, iczone))
                wDP2RO = SUM(cfDP2RO(imon, :, irri, :, iczone))
                wSF = SUM(cfSF(imon, :, irri, :, iczone))
                wRO2DP = SUM(cfRO2DP(imon, :, irri, :, iczone))
                wRO2ET= SUM(cfRO2ET(imon, :, irri, :, iczone))
    
                IF (wAcs .GT. 0.) THEN
                    IF (imon .EQ. 0) THEN
                        WRITE (10, 202) iyear, imon, iczone, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                        wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                    ELSE
                        WRITE (11, 202) iyear, imon, iczone, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                        wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                    END IF
                END IF
            END DO
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Coefficient Zone Crop - Soil
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\CoefZone\Annual\CoefZ_CropSoil_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\CoefZone\Annual\CoefZ_CropSoil_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 124)
    WRITE (11, 124)
    
    DO iczone = 1, nzones
        DO icrop = 1, ncrop
            DO isoil = 1, nsoil
                DO imon = 0, 12
                    wPrecip = SUM(cfPrecip(imon, icrop, :, isoil, iczone))
                    wAcs = SUM(cfAcs(imon, icrop, :, isoil, iczone))
                    wAP = SUM(cfAP(imon, icrop, :, isoil, iczone))
                    wSWD = SUM(cfSWD(imon, icrop, :, isoil, iczone))
                    wET = SUM(cfET(imon, icrop, :, isoil, iczone))
                    wRO = SUM(cfRO(imon, icrop, :, isoil, iczone))
                    wDP = SUM(cfDP(imon, icrop, :, isoil, iczone))
                    wSL = SUM(cfSL(imon, icrop, :, isoil, iczone))
                    wPSL = SUM(cfPSL(imon, icrop, :, isoil, iczone))
                    wDAP = SUM(cfDAP(imon, icrop, :, isoil, iczone))
                    wETG = SUM(cfETG(imon, icrop, :, isoil, iczone))
                    wDET = SUM(cfDET(imon, icrop, :, isoil, iczone))
                    wETB = SUM(cfETB(imon, icrop, :, isoil, iczone))
                    wDP1 = SUM(cfDP1(imon, icrop, :, isoil, iczone))
                    wDP2 = SUM(cfDP2(imon, icrop, :, isoil, iczone))
                    wRO1 = SUM(cfRO1(imon, icrop, :, isoil, iczone))
                    wRO2 = SUM(cfRO2(imon, icrop, :, isoil, iczone))
                    wETtrans = SUM(cfETtrans(imon, icrop, :, isoil, iczone))
                    wDP2RO = SUM(cfDP2RO(imon, icrop, :, isoil, iczone))
                    wSF = SUM(cfSF(imon, icrop, :, isoil, iczone))
                    wRO2DP = SUM(cfRO2DP(imon, icrop, :, isoil, iczone))
                    wRO2ET= SUM(cfRO2ET(imon, icrop, :, isoil, iczone))
        
                    IF (wAcs .GT. 0.) THEN
                        IF (imon .EQ. 0) THEN
                            WRITE (10, 203) iyear, imon, iczone, icrop, isoil, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                            wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                        ELSE    
                            WRITE (11, 203) iyear, imon, iczone, icrop, isoil, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                            wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                        END IF
                    END IF
                END DO
            END DO
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Coefficient Zone Crop - Irrigation Source
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\CoefZone\Annual\CoefZ_CropIrrS_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\CoefZone\Annual\CoefZ_CropIrrS_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 125)
    WRITE (11, 125)
    
    DO iczone = 1, nzones
        DO icrop = 1, ncrop
            DO irri = 1, 4
                DO imon = 0, 12
                    wPrecip = SUM(cfPrecip(imon, icrop, irri, :, iczone))
                    wAcs = SUM(cfAcs(imon, icrop, irri, :, iczone))
                    wAP = SUM(cfAP(imon, icrop, irri, :, iczone))
                    wSWD = SUM(cfSWD(imon, icrop, irri, :, iczone))
                    wET = SUM(cfET(imon, icrop, irri, :, iczone))
                    wRO = SUM(cfRO(imon, icrop, irri, :, iczone))
                    wDP = SUM(cfDP(imon, icrop, irri, :, iczone))
                    wSL = SUM(cfSL(imon, icrop, irri, :, iczone))
                    wPSL = SUM(cfPSL(imon, icrop, irri, :, iczone))
                    wDAP = SUM(cfDAP(imon, icrop, irri, :, iczone))
                    wETG = SUM(cfETG(imon, icrop, irri, :, iczone))
                    wDET = SUM(cfDET(imon, icrop, irri, :, iczone))
                    wETB = SUM(cfETB(imon, icrop, irri, :, iczone))
                    wDP1 = SUM(cfDP1(imon, icrop, irri, :, iczone))
                    wDP2 = SUM(cfDP2(imon, icrop, irri, :, iczone))
                    wRO1 = SUM(cfRO1(imon, icrop, irri, :, iczone))
                    wRO2 = SUM(cfRO2(imon, icrop, irri, :, iczone))
                    wETtrans = SUM(cfETtrans(imon, icrop, irri, :, iczone))
                    wDP2RO = SUM(cfDP2RO(imon, icrop, irri, :, iczone))
                    wSF = SUM(cfSF(imon, icrop, irri, :, iczone))
                    wRO2DP = SUM(cfRO2DP(imon, icrop, irri, :, iczone))
                    wRO2ET= SUM(cfRO2ET(imon, icrop, irri, :, iczone))
        
                    IF (wAcs .GT. 0.) THEN
                        IF (imon .EQ. 0) THEN
                            WRITE (10, 203) iyear, imon, iczone, icrop, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                            wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                        ELSE    
                            WRITE (11, 203) iyear, imon, iczone, icrop, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                            wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                        END IF
                    END IF
                END DO
            END DO
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Coefficient Zone Soil - Irrigation Source
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\CoefZone\Annual\CoefZ_SoilIrrS_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\CoefZone\Annual\CoefZ_SoilIrrS_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 126)
    WRITE (11, 126)
    
    DO iczone = 1, nzones
        DO isoil = 1, nsoil
            DO irri = 1, 4
                DO imon = 0, 12
                    wPrecip = SUM(cfPrecip(imon, :, irri, isoil, iczone))
                    wAcs = SUM(cfAcs(imon, :, irri, isoil, iczone))
                    wAP = SUM(cfAP(imon, :, irri, isoil, iczone))
                    wSWD = SUM(cfSWD(imon, :, irri, isoil, iczone))
                    wET = SUM(cfET(imon, :, irri, isoil, iczone))
                    wRO = SUM(cfRO(imon, :, irri, isoil, iczone))
                    wDP = SUM(cfDP(imon, :, irri, isoil, iczone))
                    wSL = SUM(cfSL(imon, :, irri, isoil, iczone))
                    wPSL = SUM(cfPSL(imon, :, irri, isoil, iczone))
                    wDAP = SUM(cfDAP(imon, :, irri, isoil, iczone))
                    wETG = SUM(cfETG(imon, :, irri, isoil, iczone))
                    wDET = SUM(cfDET(imon, :, irri, isoil, iczone))
                    wETB = SUM(cfETB(imon, :, irri, isoil, iczone))
                    wDP1 = SUM(cfDP1(imon, :, irri, isoil, iczone))
                    wDP2 = SUM(cfDP2(imon, :, irri, isoil, iczone))
                    wRO1 = SUM(cfRO1(imon, :, irri, isoil, iczone))
                    wRO2 = SUM(cfRO2(imon, :, irri, isoil, iczone))
                    wETtrans = SUM(cfETtrans(imon, :, irri, isoil, iczone))
                    wDP2RO = SUM(cfDP2RO(imon, :, irri, isoil, iczone))
                    wSF = SUM(cfSF(imon, :, irri, isoil, iczone))
                    wRO2DP = SUM(cfRO2DP(imon, :, irri, isoil, iczone))
                    wRO2ET= SUM(cfRO2ET(imon, :, irri, isoil, iczone))
        
                    IF (wAcs .GT. 0.) THEN
                        IF (imon .EQ. 0) THEN
                            WRITE (10, 203) iyear, imon, iczone, isoil, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                            wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                        ELSE    
                            WRITE (11, 203) iyear, imon, iczone, isoil, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                            wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                        END IF
                    END IF
                END DO
            END DO
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Coefficient Zone Crop - Soil - Irrigation Source
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\CoefZone\Annual\CoefZ_CSI_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\CoefZone\Annual\CoefZ_CSI_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 127)
    WRITE (11, 127)
    
    DO iczone = 1, nzones
        DO icrop = 1, ncrop
            DO isoil = 1, nsoil
                DO irri = 1, 4
                    DO imon = 0, 12
                        wPrecip = cfPrecip(imon, icrop, irri, isoil, iczone)
                        wAcs = cfAcs(imon, icrop, irri, isoil, iczone)
                        wAP = cfAP(imon, icrop, irri, isoil, iczone)
                        wSWD = cfSWD(imon, icrop, irri, isoil, iczone)
                        wET = cfET(imon, icrop, irri, isoil, iczone)
                        wRO = cfRO(imon, icrop, irri, isoil, iczone)
                        wDP = cfDP(imon, icrop, irri, isoil, iczone)
                        wSL = cfSL(imon, icrop, irri, isoil, iczone)
                        wPSL = cfPSL(imon, icrop, irri, isoil, iczone)
                        wDAP = cfDAP(imon, icrop, irri, isoil, iczone)
                        wETG = cfETG(imon, icrop, irri, isoil, iczone)
                        wDET = cfDET(imon, icrop, irri, isoil, iczone)
                        wETB = cfETB(imon, icrop, irri, isoil, iczone)
                        wDP1 = cfDP1(imon, icrop, irri, isoil, iczone)
                        wDP2 = cfDP2(imon, icrop, irri, isoil, iczone)
                        wRO1 = cfRO1(imon, icrop, irri, isoil, iczone)
                        wRO2 = cfRO2(imon, icrop, irri, isoil, iczone)
                        wETtrans = cfETtrans(imon, icrop, irri, isoil, iczone)
                        wDP2RO = cfDP2RO(imon, icrop, irri, isoil, iczone)
                        wSF = cfSF(imon, icrop, irri, isoil, iczone)
                        wRO2DP = cfRO2DP(imon, icrop, irri, isoil, iczone)
                        wRO2ET= cfRO2ET(imon, icrop, irri, isoil, iczone)
            
                        IF (wAcs .GT. 0.) THEN
                            IF (imon .EQ. 0) THEN
                                WRITE (10, 204) iyear, imon, iczone, icrop, isoil, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                                wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                            ELSE    
                                WRITE (11, 204) iyear, imon, iczone, icrop, isoil, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                                wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                            END IF
                        END IF
                    END DO
                END DO
            END DO
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Runoff Zone Totals
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\ROZone\Annual\ROZ_Tot_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\ROZone\Annual\ROZ_Tot_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 130)
    WRITE (11, 130)
    
    DO irzone = 0, rzones
        DO imon = 0, 12
            wPrecip = SUM(rzPrecip(imon, :, :, :, irzone))
            wAcs = SUM(rzAcs(imon, :, :, :, irzone))
            wAP = SUM(rzAP(imon, :, :, :, irzone))
            wSWD = SUM(rzSWD(imon, :, :, :, irzone))
            wET = SUM(rzET(imon, :, :, :, irzone))
            wRO = SUM(rzRO(imon, :, :, :, irzone))
            wDP = SUM(rzDP(imon, :, :, :, irzone))
            wSL = SUM(rzSL(imon, :, :, :, irzone))
            wPSL = SUM(rzPSL(imon, :, :, :, irzone))
            wDAP = SUM(rzDAP(imon, :, :, :, irzone))
            wETG = SUM(rzETG(imon, :, :, :, irzone))
            wDET = SUM(rzDET(imon, :, :, :, irzone))
            wETB = SUM(rzETB(imon, :, :, :, irzone))
            wDP1 = SUM(rzDP1(imon, :, :, :, irzone))
            wDP2 = SUM(rzDP2(imon, :, :, :, irzone))
            wRO1 = SUM(rzRO1(imon, :, :, :, irzone))
            wRO2 = SUM(rzRO2(imon, :, :, :, irzone))
            wETtrans = SUM(rzETtrans(imon, :, :, :, irzone))
            wDP2RO = SUM(rzDP2RO(imon, :, :, :, irzone))
            wSF = SUM(rzSF(imon, :, :, :, irzone))
            wRO2DP = SUM(rzRO2DP(imon, :, :, :, irzone))
            wRO2ET= SUM(rzRO2ET(imon, :, :, :, irzone))
    
            IF (wAcs .GT. 0.) THEN
                IF (imon .EQ. 0) THEN
                    WRITE (10, 201) iyear, imon, irzone, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                    wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                ELSE
                    WRITE (11, 201) iyear, imon, irzone, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                    wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                END IF
            END IF
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Runoff Zone Crop
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\ROZone\Annual\ROZ_Crop_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\ROZone\Annual\ROZ_Crop_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 131)
    WRITE (11, 131)
    
    DO irzone = 0, rzones
        DO icrop = 1, ncrop
            DO imon = 0, 12
                wPrecip = SUM(rzPrecip(imon, icrop, :, :, irzone))
                wAcs = SUM(rzAcs(imon, icrop, :, :, irzone))
                wAP = SUM(rzAP(imon, icrop, :, :, irzone))
                wSWD = SUM(rzSWD(imon, icrop, :, :, irzone))
                wET = SUM(rzET(imon, icrop, :, :, irzone))
                wRO = SUM(rzRO(imon, icrop, :, :, irzone))
                wDP = SUM(rzDP(imon, icrop, :, :, irzone))
                wSL = SUM(rzSL(imon, icrop, :, :, irzone))
                wPSL = SUM(rzPSL(imon, icrop, :, :, irzone))
                wDAP = SUM(rzDAP(imon, icrop, :, :, irzone))
                wETG = SUM(rzETG(imon, icrop, :, :, irzone))
                wDET = SUM(rzDET(imon, icrop, :, :, irzone))
                wETB = SUM(rzETB(imon, icrop, :, :, irzone))
                wDP1 = SUM(rzDP1(imon, icrop, :, :, irzone))
                wDP2 = SUM(rzDP2(imon, icrop, :, :, irzone))
                wRO1 = SUM(rzRO1(imon, icrop, :, :, irzone))
                wRO2 = SUM(rzRO2(imon, icrop, :, :, irzone))
                wETtrans = SUM(rzETtrans(imon, icrop, :, :, irzone))
                wDP2RO = SUM(rzDP2RO(imon, icrop, :, :, irzone))
                wSF = SUM(rzSF(imon, icrop, :, :, irzone))
                wRO2DP = SUM(rzRO2DP(imon, icrop, :, :, irzone))
                wRO2ET= SUM(rzRO2ET(imon, icrop, :, :, irzone))
    
                IF (wAcs .GT. 0.) THEN
                    IF (imon .EQ. 0) THEN
                        WRITE (10, 202) iyear, imon, irzone, icrop, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                        wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                    ELSE
                        WRITE (11, 202) iyear, imon, irzone, icrop, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                        wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                    END IF
                END IF
            END DO
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Runoff Zone Soil
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\ROZone\Annual\ROZ_Soil_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\ROZone\Annual\ROZ_Soil_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 132)
    WRITE (11, 132)
    
    DO irzone = 0, rzones
        DO isoil = 1, nsoil
            DO imon = 0, 12
                wPrecip = SUM(rzPrecip(imon, :, :, isoil, irzone))
                wAcs = SUM(rzAcs(imon, :, :, isoil, irzone))
                wAP = SUM(rzAP(imon, :, :, isoil, irzone))
                wSWD = SUM(rzSWD(imon, :, :, isoil, irzone))
                wET = SUM(rzET(imon, :, :, isoil, irzone))
                wRO = SUM(rzRO(imon, :, :, isoil, irzone))
                wDP = SUM(rzDP(imon, :, :, isoil, irzone))
                wSL = SUM(rzSL(imon, :, :, isoil, irzone))
                wPSL = SUM(rzPSL(imon, :, :, isoil, irzone))
                wDAP = SUM(rzDAP(imon, :, :, isoil, irzone))
                wETG = SUM(rzETG(imon, :, :, isoil, irzone))
                wDET = SUM(rzDET(imon, :, :, isoil, irzone))
                wETB = SUM(rzETB(imon, :, :, isoil, irzone))
                wDP1 = SUM(rzDP1(imon, :, :, isoil, irzone))
                wDP2 = SUM(rzDP2(imon, :, :, isoil, irzone))
                wRO1 = SUM(rzRO1(imon, :, :, isoil, irzone))
                wRO2 = SUM(rzRO2(imon, :, :, isoil, irzone))
                wETtrans = SUM(rzETtrans(imon, :, :, isoil, irzone))
                wDP2RO = SUM(rzDP2RO(imon, :, :, isoil, irzone))
                wSF = SUM(rzSF(imon, :, :, isoil, irzone))
                wRO2DP = SUM(rzRO2DP(imon, :, :, isoil, irzone))
                wRO2ET= SUM(rzRO2ET(imon, :, :, isoil, irzone))
    
                IF (wAcs .GT. 0.) THEN
                    IF (imon .EQ. 0) THEN
                        WRITE (10, 202) iyear, imon, irzone, isoil, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                        wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                    ELSE
                        WRITE (11, 202) iyear, imon, irzone, isoil, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                        wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                    END IF
                END IF
            END DO
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Runoff Zone Irrigation Source
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\ROZone\Annual\ROZ_IrrS_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\ROZone\Annual\ROZ_IrrS_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 133)
    WRITE (11, 133)
    
    DO irzone = 0, rzones
        DO irri = 1, 4
            DO imon = 0, 12
                wPrecip = SUM(rzPrecip(imon, :, irri, :, irzone))
                wAcs = SUM(rzAcs(imon, :, irri, :, irzone))
                wAP = SUM(rzAP(imon, :, irri, :, irzone))
                wSWD = SUM(rzSWD(imon, :, irri, :, irzone))
                wET = SUM(rzET(imon, :, irri, :, irzone))
                wRO = SUM(rzRO(imon, :, irri, :, irzone))
                wDP = SUM(rzDP(imon, :, irri, :, irzone))
                wSL = SUM(rzSL(imon, :, irri, :, irzone))
                wPSL = SUM(rzPSL(imon, :, irri, :, irzone))
                wDAP = SUM(rzDAP(imon, :, irri, :, irzone))
                wETG = SUM(rzETG(imon, :, irri, :, irzone))
                wDET = SUM(rzDET(imon, :, irri, :, irzone))
                wETB = SUM(rzETB(imon, :, irri, :, irzone))
                wDP1 = SUM(rzDP1(imon, :, irri, :, irzone))
                wDP2 = SUM(rzDP2(imon, :, irri, :, irzone))
                wRO1 = SUM(rzRO1(imon, :, irri, :, irzone))
                wRO2 = SUM(rzRO2(imon, :, irri, :, irzone))
                wETtrans = SUM(rzETtrans(imon, :, irri, :, irzone))
                wDP2RO = SUM(rzDP2RO(imon, :, irri, :, irzone))
                wSF = SUM(rzSF(imon, :, irri, :, irzone))
                wRO2DP = SUM(rzRO2DP(imon, :, irri, :, irzone))
                wRO2ET= SUM(rzRO2ET(imon, :, irri, :, irzone))
    
                IF (wAcs .GT. 0.) THEN
                    IF (imon .EQ. 0) THEN
                        WRITE (10, 202) iyear, imon, irzone, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                        wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                    ELSE
                        WRITE (11, 202) iyear, imon, irzone, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                        wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                    END IF
                END IF
            END DO
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Runoff Zone Crop - Soil
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\ROZone\Annual\ROZ_CropSoil_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\ROZone\Annual\ROZ_CropSoil_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 134)
    WRITE (11, 134)
    
    DO irzone = 0, rzones
        DO icrop = 1, ncrop
            DO isoil = 1, nsoil
                DO imon = 0, 12
                    wPrecip = SUM(rzPrecip(imon, icrop, :, isoil, irzone))
                    wAcs = SUM(rzAcs(imon, icrop, :, isoil, irzone))
                    wAP = SUM(rzAP(imon, icrop, :, isoil, irzone))
                    wSWD = SUM(rzSWD(imon, icrop, :, isoil, irzone))
                    wET = SUM(rzET(imon, icrop, :, isoil, irzone))
                    wRO = SUM(rzRO(imon, icrop, :, isoil, irzone))
                    wDP = SUM(rzDP(imon, icrop, :, isoil, irzone))
                    wSL = SUM(rzSL(imon, icrop, :, isoil, irzone))
                    wPSL = SUM(rzPSL(imon, icrop, :, isoil, irzone))
                    wDAP = SUM(rzDAP(imon, icrop, :, isoil, irzone))
                    wETG = SUM(rzETG(imon, icrop, :, isoil, irzone))
                    wDET = SUM(rzDET(imon, icrop, :, isoil, irzone))
                    wETB = SUM(rzETB(imon, icrop, :, isoil, irzone))
                    wDP1 = SUM(rzDP1(imon, icrop, :, isoil, irzone))
                    wDP2 = SUM(rzDP2(imon, icrop, :, isoil, irzone))
                    wRO1 = SUM(rzRO1(imon, icrop, :, isoil, irzone))
                    wRO2 = SUM(rzRO2(imon, icrop, :, isoil, irzone))
                    wETtrans = SUM(rzETtrans(imon, icrop, :, isoil, irzone))
                    wDP2RO = SUM(rzDP2RO(imon, icrop, :, isoil, irzone))
                    wSF = SUM(rzSF(imon, icrop, :, isoil, irzone))
                    wRO2DP = SUM(rzRO2DP(imon, icrop, :, isoil, irzone))
                    wRO2ET= SUM(rzRO2ET(imon, icrop, :, isoil, irzone))
        
                    IF (wAcs .GT. 0.) THEN
                        IF (imon .EQ. 0) THEN
                            WRITE (10, 203) iyear, imon, irzone, icrop, isoil, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                            wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                        ELSE    
                            WRITE (11, 203) iyear, imon, irzone, icrop, isoil, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                            wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                        END IF
                    END IF
                END DO
            END DO
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Runoff Zone Crop - Irrigation Source
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\ROZone\Annual\ROZ_CropIrrS_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\ROZone\Annual\ROZ_CropIrrS_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 135)
    WRITE (11, 135)
    
    DO irzone = 0, rzones
        DO icrop = 1, ncrop
            DO irri = 1, 4
                DO imon = 0, 12
                    wPrecip = SUM(rzPrecip(imon, icrop, irri, :, irzone))
                    wAcs = SUM(rzAcs(imon, icrop, irri, :, irzone))
                    wAP = SUM(rzAP(imon, icrop, irri, :, irzone))
                    wSWD = SUM(rzSWD(imon, icrop, irri, :, irzone))
                    wET = SUM(rzET(imon, icrop, irri, :, irzone))
                    wRO = SUM(rzRO(imon, icrop, irri, :, irzone))
                    wDP = SUM(rzDP(imon, icrop, irri, :, irzone))
                    wSL = SUM(rzSL(imon, icrop, irri, :, irzone))
                    wPSL = SUM(rzPSL(imon, icrop, irri, :, irzone))
                    wDAP = SUM(rzDAP(imon, icrop, irri, :, irzone))
                    wETG = SUM(rzETG(imon, icrop, irri, :, irzone))
                    wDET = SUM(rzDET(imon, icrop, irri, :, irzone))
                    wETB = SUM(rzETB(imon, icrop, irri, :, irzone))
                    wDP1 = SUM(rzDP1(imon, icrop, irri, :, irzone))
                    wDP2 = SUM(rzDP2(imon, icrop, irri, :, irzone))
                    wRO1 = SUM(rzRO1(imon, icrop, irri, :, irzone))
                    wRO2 = SUM(rzRO2(imon, icrop, irri, :, irzone))
                    wETtrans = SUM(rzETtrans(imon, icrop, irri, :, irzone))
                    wDP2RO = SUM(rzDP2RO(imon, icrop, irri, :, irzone))
                    wSF = SUM(rzSF(imon, icrop, irri, :, irzone))
                    wRO2DP = SUM(rzRO2DP(imon, icrop, irri, :, irzone))
                    wRO2ET= SUM(rzRO2ET(imon, icrop, irri, :, irzone))
        
                    IF (wAcs .GT. 0.) THEN
                        IF (imon .EQ. 0) THEN
                            WRITE (10, 203) iyear, imon, irzone, icrop, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                            wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                        ELSE    
                            WRITE (11, 203) iyear, imon, irzone, icrop, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                            wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                        END IF
                    END IF
                END DO
            END DO
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Runoff Zone Soil - Irrigation Source
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\ROZone\Annual\ROZ_SoilIrrS_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\ROZone\Annual\ROZ_SoilIrrS_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 136)
    WRITE (11, 136)
    
    DO irzone = 0, rzones
        DO isoil = 1, nsoil
            DO irri = 1, 4
                DO imon = 0, 12
                    wPrecip = SUM(rzPrecip(imon, :, irri, isoil, irzone))
                    wAcs = SUM(rzAcs(imon, :, irri, isoil, irzone))
                    wAP = SUM(rzAP(imon, :, irri, isoil, irzone))
                    wSWD = SUM(rzSWD(imon, :, irri, isoil, irzone))
                    wET = SUM(rzET(imon, :, irri, isoil, irzone))
                    wRO = SUM(rzRO(imon, :, irri, isoil, irzone))
                    wDP = SUM(rzDP(imon, :, irri, isoil, irzone))
                    wSL = SUM(rzSL(imon, :, irri, isoil, irzone))
                    wPSL = SUM(rzPSL(imon, :, irri, isoil, irzone))
                    wDAP = SUM(rzDAP(imon, :, irri, isoil, irzone))
                    wETG = SUM(rzETG(imon, :, irri, isoil, irzone))
                    wDET = SUM(rzDET(imon, :, irri, isoil, irzone))
                    wETB = SUM(rzETB(imon, :, irri, isoil, irzone))
                    wDP1 = SUM(rzDP1(imon, :, irri, isoil, irzone))
                    wDP2 = SUM(rzDP2(imon, :, irri, isoil, irzone))
                    wRO1 = SUM(rzRO1(imon, :, irri, isoil, irzone))
                    wRO2 = SUM(rzRO2(imon, :, irri, isoil, irzone))
                    wETtrans = SUM(rzETtrans(imon, :, irri, isoil, irzone))
                    wDP2RO = SUM(rzDP2RO(imon, :, irri, isoil, irzone))
                    wSF = SUM(rzSF(imon, :, irri, isoil, irzone))
                    wRO2DP = SUM(rzRO2DP(imon, :, irri, isoil, irzone))
                    wRO2ET= SUM(rzRO2ET(imon, :, irri, isoil, irzone))
        
                    IF (wAcs .GT. 0.) THEN
                        IF (imon .EQ. 0) THEN
                            WRITE (10, 203) iyear, imon, irzone, isoil, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                            wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                        ELSE    
                            WRITE (11, 203) iyear, imon, irzone, isoil, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                            wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                        END IF
                    END IF
                END DO
            END DO
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
    !Runoff Zone Crop - Soil - Irrigation Source
    OPEN (10, FILE = TRIM(OUTDIR)//'Report\ROZone\Annual\ROZ_CSI_ann'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    OPEN (11, FILE = TRIM(OUTDIR)//'Report\ROZone\Annual\ROZ_CSI_mon'//TRIM(YRT)//'.txt', STATUS = 'UNKNOWN')
    WRITE (10, 137)
    WRITE (11, 137)
    
    DO irzone = 0, rzones
        DO icrop = 1, ncrop
            DO isoil = 1, nsoil
                DO irri = 1, 4
                    DO imon = 0, 12
                        wPrecip = rzPrecip(imon, icrop, irri, isoil, irzone)
                        wAcs = rzAcs(imon, icrop, irri, isoil, irzone)
                        wAP = rzAP(imon, icrop, irri, isoil, irzone)
                        wSWD = rzSWD(imon, icrop, irri, isoil, irzone)
                        wET = rzET(imon, icrop, irri, isoil, irzone)
                        wRO = rzRO(imon, icrop, irri, isoil, irzone)
                        wDP = rzDP(imon, icrop, irri, isoil, irzone)
                        wSL = rzSL(imon, icrop, irri, isoil, irzone)
                        wPSL = rzPSL(imon, icrop, irri, isoil, irzone)
                        wDAP = rzDAP(imon, icrop, irri, isoil, irzone)
                        wETG = rzETG(imon, icrop, irri, isoil, irzone)
                        wDET = rzDET(imon, icrop, irri, isoil, irzone)
                        wETB = rzETB(imon, icrop, irri, isoil, irzone)
                        wDP1 = rzDP1(imon, icrop, irri, isoil, irzone)
                        wDP2 = rzDP2(imon, icrop, irri, isoil, irzone)
                        wRO1 = rzRO1(imon, icrop, irri, isoil, irzone)
                        wRO2 = rzRO2(imon, icrop, irri, isoil, irzone)
                        wETtrans = rzETtrans(imon, icrop, irri, isoil, irzone)
                        wDP2RO = rzDP2RO(imon, icrop, irri, isoil, irzone)
                        wSF = rzSF(imon, icrop, irri, isoil, irzone)
                        wRO2DP = rzRO2DP(imon, icrop, irri, isoil, irzone)
                        wRO2ET= rzRO2ET(imon, icrop, irri, isoil, irzone)
            
                        IF (wAcs .GT. 0.) THEN
                            IF (imon .EQ. 0) THEN
                                WRITE (10, 204) iyear, imon, irzone, icrop, isoil, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                                wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                            ELSE    
                                WRITE (11, 204) iyear, imon, irzone, icrop, isoil, irri, wAcs, wPrecip, wAP, wSWD, wET, wRO, wDP, wSL, wPSL, wDAP, &
                                                wETG, wDET, wETB, wDP1, wDP2, wRO1, wRO2, wETtrans, wDP2RO, wSF, wRO2DP, wRO2ET
                            END IF
                        END IF
                    END DO
                END DO
            END DO
        END DO
    END DO
    CLOSE (10)
    CLOSE (11)
    
100 FORMAT ('Year, Month, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
101 FORMAT ('Year, Month, Crop, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
102 FORMAT ('Year, Month, Soil, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
103 FORMAT ('Year, Month, IrrS, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
104 FORMAT ('Year, Month, Crop, Soil, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
105 FORMAT ('Year, Month, Crop, IrrS, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
106 FORMAT ('Year, Month, Soil, IrrS, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
107 FORMAT ('Year, Month, Crop, Soil, IrrS, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
    
110 FORMAT ('Year, Month, County_Index, County, State, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
111 FORMAT ('Year, Month, County_Index, County, State, Crop, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
112 FORMAT ('Year, Month, County_Index, County, State, Soil, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
113 FORMAT ('Year, Month, County_Index, County, State, IrrS, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
114 FORMAT ('Year, Month, County_Index, County, State, Crop, Soil, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
115 FORMAT ('Year, Month, County_Index, County, State, Crop, IrrS, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
116 FORMAT ('Year, Month, County_Index, County, State, Soil, IrrS, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
117 FORMAT ('Year, Month, County_Index, County, State, Crop, Soil, IrrS, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
    
120 FORMAT ('Year, Month, CoefZ, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
121 FORMAT ('Year, Month, CoefZ, Crop, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
122 FORMAT ('Year, Month, CoefZ, Soil, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
123 FORMAT ('Year, Month, CoefZ, IrrS, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
124 FORMAT ('Year, Month, CoefZ, Crop, Soil, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
125 FORMAT ('Year, Month, CoefZ, Crop, IrrS, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
126 FORMAT ('Year, Month, CoefZ, Soil, IrrS, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
127 FORMAT ('Year, Month, CoefZ, Crop, Soil, IrrS, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')

130 FORMAT ('Year, Month, ROZ, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
131 FORMAT ('Year, Month, ROZ, Crop, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
132 FORMAT ('Year, Month, ROZ, Soil, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
133 FORMAT ('Year, Month, ROZ, IrrS, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
134 FORMAT ('Year, Month, ROZ, Crop, Soil, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
135 FORMAT ('Year, Month, ROZ, Crop, IrrS, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
136 FORMAT ('Year, Month, ROZ, Soil, IrrS, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
137 FORMAT ('Year, Month, ROZ, Crop, Soil, IrrS, Acres, Precip, Pumping, Deliveries, ET, RO, DP, SL, PSL, DAP, ' &
            'ETG, DET, ETb, DP1, DP2, RO1, RO2, ETtrans, DP2RO, SF, RO2DP, RO2ET')
    
200 FORMAT (I5, ',', I3, 22(',', F14.4))
201 FORMAT (I5, 2(',', I3), 22(',', F14.4))
202 FORMAT (I5, 3(',', I3), 22(',', F14.4))
203 FORMAT (I5, 4(',', I3), 22(',', F14.4))
204 FORMAT (I5, 5(',', I3), 22(',', F14.4))
    
210 FORMAT (I5, 2(',', I3), 2(',', A14), 22(',', F14.4))
211 FORMAT (I5, 2(',', I3), 2(',', A14), ',', I3, 22(',', F14.4))
212 FORMAT (I5, 2(',', I3), 2(',', A14), 2(',', I3), 22(',', F14.4))
213 FORMAT (I5, 2(',', I3), 2(',', A14), 3(',', I3), 22(',', F14.4))
    
END SUBROUTINE writereports
!************************************************************************