module parMakeRCH
    CHARACTER*600    OUTDIR2
    CHARACTER*35    rchfile, rchfold
    
    INTEGER     fcol, pcol
    INTEGER     startmon, endmon, mon
    INTEGER     zz, irow, icol, linenum
    
    
    REAL, allocatable ::    IrrPrecRch(:, :), IrrPrecRO(:, :), CellRCH(:, :)
    REAL, allocatable ::    DP(:), RO(:), Canalrch(:, :), cnlr(:)
    REAL, allocatable ::    Miscrch(:, :), mrch(:)
  
end module

subroutine Make_RCH
    use parMakeRCH
    use Param

    IMPLICIT NONE
    
    
    
    allocate(IrrPrecRch(ncell, 0 : 12), IrrPrecRO(ncell, 0 : 12), CellRCH(nrows, ncols))
    allocate(DP(0 : 12), RO(0 : 12), Canalrch(ncell, 0 : 12), cnlr(0 : 12))
    allocate(Miscrch(ncell, 0 : 12), mrch(0 : 12))
    
    WRITE (output_unit, '(A)') 'MakeRCH'
    
    OUTDIR2 = trim(OUTDIR)
    fcol = ncols / 10
    pcol = MOD(ncols, 10)
    
    !Populate Cells with Location Variables
    if (.not. allocated(CoefZone)) call errexit('must define CELLLOC before MAKERCH')
    
    !Populate Runoff Zone Coefs
    if (.not. allocated(LPM)) call errexit('must define COEFFRO before MAKERCH')
	Close (9)

    !LOAD CELL COEFFICIENTS
    if (.not. allocated(DryETadj)) call errexit('must define COEFF before MAKERCH')
    
    call openoutfile(10, TRIM(OUTDIR2), 'Rch_values.txt')
    WRITE (10, 100)
    
    DO iyear = startyr, endyr
        WRITE(output_unit, '(I6, I8)') iyear, cyr(iyear)
        WRITE(YRT, '(I4)') iyear
        
        IF (iyear .LT. SwcYr) THEN
            startmon = 1
            endmon = 1
        ELSE
            startmon = 1
            endmon = 12
        END IF
        
        IF ( MOD(iyear, 4) .EQ. 0) THEN
            DaysInMonth(2) = 29
            DIY = 366
        ELSE
            DaysInMonth(2) = 28
            DIY = 365
        END IF
        
        
        IrrPrecRch = 0.
        IrrPrecRO = 0.
        
        OPEN (1, FILE = TRIM(OUTDIR)//'WSPP_OUT\WSPPOut'//TRIM(YRT)//'.txt', STATUS = 'OLD')
        READ (1, *)
        DO WHILE (.NOT. EOF(1))
            READ (1, *) Cell, yr, DP(:), RO(:)
            IrrPrecRch(Cell, :) = IrrPrecRch(Cell, :) + DP(:)
            IrrPrecRO(Cell, :) = IrrPrecRO(Cell, :) + RO(:)
        END DO
        CLOSE(1)
        
        canalrch = 0.
        IF (CnlRch .EQ. 1) THEN
            OPEN (2, FILE = TRIM(CNLDIR), STATUS = 'OLD')
            DO WHILE (.NOT. EOF(2))
                READ (2, *) Rchfold, Rchfile
                
                OPEN (1, FILE = TRIM(Rchfold)//'\'//TRIM(rchfile)//TRIM(YRT)//'.txt', STATUS = 'OLD')
                READ (1, *)
                DO WHILE (.NOT. EOF(1))
                    READ (1, *)  Cell, yr, cnlr(:)
                    canalrch(cell, :) = canalrch(cell, :) + cnlr(:)
                END DO
                CLOSE (1)
            END DO
            CLOSE (2)
        END IF
        
        miscrch = 0.
        IF (mscrch .EQ. 1) THEN
            OPEN (1, FILE = TRIM(MiscDIR), STATUS = 'OLD')
            DO WHILE (.NOT. EOF(1))
                READ (1, *) Rchfold, Rchfile
                
                OPEN (2, FILE = TRIM(Rchfold)//'\'//TRIM(Rchfile)//TRIM(YRT)//'.txt', STATUS = 'OLD')
                READ (2, *)
                DO WHILE (.NOT. EOF(2))
                    READ(2, *) Cell, yr, mrch(:)
                    miscrch(Cell, :) = miscrch(Cell, :) + mrch(:)
                END DO
                CLOSE(2)
            END DO
            CLOSE(1)
        END IF
        
        call openoutfile(20, TRIM(OUTDIR2)//'RCH','UNWNRD'//TRIM(YRT)//'.RCH')
        call openoutfile(30, TRIM(OUTDIR2)//'IrrPrec','IrrPrec'//TRIM(YRT)//'.txt')
        call openoutfile(40, TRIM(OUTDIR2)//'Canal','Canal'//TRIM(YRT)//'.txt')
        call openoutfile(50, TRIM(OUTDIR2)//'SF','SF'//TRIM(YRT)//'.txt')
        call openoutfile(60, TRIM(OUTDIR2)//'ROBal','ROBal'//TRIM(YRT)//'.txt')
        call openoutfile(70, TRIM(OUTDIR2)//'AnnRCH','AnnRCH'//TRIM(YRT)//'.txt')
      
        
        WRITE (30, 131)
        WRITE (40, 141)
        WRITE (50, 151)
        WRITE (60, 161)
        WRITE (70, 171)
        
        DO Mon = startmon, endmon
            !WRITE (6, *) mon
            CellRch = 0.
            CALL popcellrch
            
            WRITE(20, 122)
            WRITE(20, 123)
            
            zz = 1
            DO irow = 1, nrows
                IF(zz .GT. ncell) GOTO 16
                
                IF (irow .EQ. 305) THEN
                    CONTINUE
                END IF                
                
                icol = 1
                DO Linenum = 1, fcol
                    DO k = 0, 8
                        IF (zz .GT. ncell) THEN
                            WRITE (20, *)
                            GOTO 16
                        END IF
                        
                        IF (Cellrch(irow, icol + k) .EQ. 0.) THEN
                            WRITE (20, 204) Cellrch(irow, icol + k)
                            zz = zz + 1
                        ELSE IF(Cellrch(irow, icol + k) .LE. 10.) THEN
                            WRITE (20, 215) Cellrch(irow, icol + k)
                            zz = zz + 1
                        ELSE IF(Cellrch(irow, icol + k) .LE. 100.) THEN
                            WRITE (20, 216) Cellrch(irow, icol + k)
                            zz = zz + 1
                        ELSE IF(Cellrch(irow, icol + k) .LE. 1000.) THEN
                            WRITE (20, 217) Cellrch(irow, icol + k)
                            zz = zz + 1
                        ELSE IF(Cellrch(irow, icol + k) .LE. 10000.) THEN
                            WRITE (20, 218) Cellrch(irow, icol + k)
                            zz = zz + 1
                        ELSE
                            WRITE (20, 219) Cellrch(irow, icol + k)
                            zz = zz + 1
                        END IF
                    END DO
                    
                    IF(zz .GT. ncell) THEN
                        WRITE (20, *)
                        GOTO 16
                    END IF
                    
                    k = 9
                    IF (Cellrch(irow, icol + k) .EQ. 0.) THEN
                        WRITE (20, 206) Cellrch(irow, icol + k)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, icol + k) .LE. 10.) THEN
                        WRITE (20, 225) Cellrch(irow, icol + k)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, icol + k) .LE. 100.) THEN
                        WRITE (20, 226) Cellrch(irow, icol + k)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, icol + k) .LE. 1000.) THEN
                        WRITE (20, 227) Cellrch(irow, icol + k)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, icol + k) .LE. 10000.) THEN
                        WRITE (20, 228) Cellrch(irow, icol + k)
                        zz = zz + 1
                    ELSE
                        WRITE (20, 229) Cellrch(irow, icol + k)
                        zz = zz + 1
                    END IF
                    icol = icol + 10
                END DO
                
                DO k = 1, pcol - 1
                    IF (zz .GT. ncell) THEN
                        WRITE(20, *)
                        GOTO 16
                    END IF
                    
                    IF (Cellrch(irow, (fcol * 10) + k) .EQ. 0.) THEN
                        WRITE (20, 204) Cellrch(irow, (fcol * 10) + k)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, (fcol * 10) + k) .LE. 10.) THEN
                        WRITE (20, 215) Cellrch(irow, (fcol * 10) + k)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, (fcol * 10) + k) .LE. 100.) THEN
                        WRITE (20, 216) Cellrch(irow, (fcol * 10) + k)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, (fcol * 10) + k) .LE. 1000.) THEN
                        WRITE (20, 217) Cellrch(irow, (fcol * 10) + k)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, (fcol * 10) + k) .LE. 10000.) THEN
                        WRITE (20, 218) Cellrch(irow, (fcol * 10) + k)
                        zz = zz + 1
                    ELSE
                        WRITE (20, 219) Cellrch(irow, (fcol * 10) + k)
                        zz = zz + 1
                    END IF
                END DO
                
                IF(zz .GT. ncell) THEN
                    IF (pcol .NE. 0) WRITE(20, *)
                    GOTO 16
                END IF
                
                IF (pcol .NE. 0) THEN
                    IF (Cellrch(irow, ncols) .EQ. 0.) THEN
                        WRITE (20, 206) Cellrch(irow, ncols)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, ncols) .LE. 10.) THEN
                        WRITE (20, 225) Cellrch(irow, ncols)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, ncols) .LE. 100.) THEN
                        WRITE (20, 226) Cellrch(irow, ncols)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, ncols) .LE. 1000.) THEN
                        WRITE (20, 227) Cellrch(irow, ncols)
                        zz = zz + 1
                    ELSE IF(Cellrch(irow, ncols) .LE. 10000.) THEN
                        WRITE (20, 228) Cellrch(irow, ncols)
                        zz = zz + 1
                    ELSE
                        WRITE (20, 229) Cellrch(irow, ncols)
                        zz = zz + 1
                    END IF
                END IF
16          END DO
        END DO
        
        CLOSE (20)
        CLOSE (30)
        CLOSE (40)
        CLOSE (50)
        CLOSE (60)
        CLOSE (70)
        
    END DO
    
    CLOSE (10)
    
122 FORMAT ('         1         1')
123 FORMAT ('INTERNAL 1.0 (free) 0')
    
204 FORMAT (F3.1, ' ', \) 
206 FORMAT (F3.1)
   
215 FORMAT (F14.12, ' ', \)
216 FORMAT (F15.12, ' ', \)
217 FORMAT (F16.12, ' ', \)
218 FORMAT (F17.12, ' ', \)
219 FORMAT (F18.12, ' ', \)
   
225 FORMAT (F14.12)
226 FORMAT (F15.12)
227 FORMAT (F16.12)
228 FORMAT (F17.12)
229 FORMAT (F18.12)
   
131 FORMAT ( 'Cell, ROZone, Row, Col, Year, Month, NumberOfDaysInMonth, MonthlyIrrPrecipDP_AF, MonthlyTransDP_AF, MonthlyCanalRchg_AF, ' &
             'Misc Pump Recharge, LPM, MiToGauge, PerToRchg, TotalMonthlyDP_FTperDAY, MonthlyIrrPrecipRchg_FTperDAY') !Header (File 30)
141 FORMAT ( 'Cell, QtrCellRow, QtrCellCol, Year, Month, NumberOfDaysInMonth, QtrCellCanalRchg_AFPerMonth, QtrCellCanalRchg_FTPerDay')
151	FORMAT ( 'Cell, ROZone, Row, Col, Year, Month, NumberOfDaysInMonth, AnnualSF_AF, AnnualCellRO_AF, LPM, MiToGauge, MonthlySF_AFperDAY') !Header (File 50)
161 FORMAT ( 'Cell, Year, Month, FieldRO_AF, TransRchg_AF, SF_AF, ET_AF')
171 FORMAT ( 'Cell, Year, Month, Total_Recharge, Direct_RCH, EOF_RCH, Canal_RCH, Misc_Pump_RCH, Stream_Flow, RO_Transfer_ET, Direct_RO')
100 FORMAT ( 'Cell, Year, Rch')
 
    

END subroutine Make_RCH
!*****************************************************************************
SUBROUTINE popcellrch()
    use parMakeRCH
    USE Param
    IMPLICIT NONE
    DOUBLE PRECISION TotRch
    
    INTEGER     row, col
    
    REAL        LF
    REAL        SF
    REAL        RO2DP
    REAL        RO2ET
    associate(PctRch => PertoRchg)
      
    DO Cell = 1, ncell
        col = MOD(Cell, ncols)
        IF (col .EQ. 0) col = ncols
        row = (Cell - col) / ncols + 1
        
        IF (MiToGauge(cell) .EQ. 0.) THEN
            LF = 0.5
        ELSE
            LF = MIN(1 - EXP(-LPM(ROZone(cell)) * MiToGauge(Cell)), 1.0)
        END IF
        
        IF (mon .EQ. 1) THEN
            
            SF = SUM(IrrPrecRO(cell, 1 : 12)) * (1 - LF)
            RO2DP = SUM(IrrPrecRO(cell, 1 : 12)) * LF * PctRch(coefzone(cell))
            RO2ET = SUM(IrrPrecRO(cell, 1 : 12)) * LF * (1 - PctRch(coefzone(cell)))
            
            TotRch = SUM(IrrPrecRch(cell, 1 : 12)) + RO2DP + SUM(Canalrch(Cell, 1 : 12)) + SUM(Miscrch(cell, 1 : 12))
            
            IF (TotRch .GT. 0.) THEN
                WRITE (70, 173) Cell, iyear, totrch, SUM(IrrPrecRch(cell, 1 : 12)), RO2DP, SUM(Canalrch(cell, 1 : 12)), &
                                    SUM(Miscrch(cell, 1 : 12)), SF, RO2ET, SUM(IrrPrecRO(cell, 1 : 12))
                
                IF (iyear .LT. SwcYr) THEN
                    Cellrch(row, col) = TotRch / csize / DIY
                END IF
                
            END IF
            
            WRITE (10, 101) Cell, iyear, MAX(TotRch, 0.) / csize * 12
            
        END IF
        
        SF = IrrPrecRO(Cell, mon) * (1 - LF)
        RO2DP = IrrPrecRO(Cell, mon) * LF * PctRch(coefzone(cell))
        RO2ET = IrrPrecRO(Cell, mon) * LF * (1 -PctRch(coefzone(cell)))
        
        IF (cell .EQ. 85863) THEN
            CONTINUE
        END IF
        
        TotRch = IrrPrecRch(Cell, mon) + RO2DP + Canalrch(Cell, Mon) + Miscrch(Cell, Mon)
        
        IF (TotRch .GT. 0.) THEN
            WRITE (70, 172) Cell, iyear, mon, TotRch, IrrPrecRch(Cell, mon), RO2DP, Canalrch(cell, mon), Miscrch(cell, mon), &
                                SF, RO2ET, IrrPrecRO(Cell, mon)
            
            IF (iyear .GE. SwcYr) THEN
                Cellrch(row, col) = TotRch / csize / DaysInMonth(mon)
            END IF
            
            WRITE (30, 132) Cell, Rozone(cell), row, col, iyear, mon, DaysInMonth(mon), &
                                IrrPrecRch(cell, mon), RO2DP, Canalrch(cell, mon), miscrch(cell, mon), &
                                LPM(ROZone(cell)), MiToGauge(cell), PctRch(Coefzone(cell)), Cellrch(row, col), &
                                Cellrch(row, col) - Canalrch(cell, mon) / csize / DaysInMonth(mon)
            
            IF (Canalrch(cell, mon) .GT. 0.) THEN
                WRITE(40, 142) Cell, row, col, iyear, mon, DaysInMonth(mon), Canalrch(cell, mon), &
                                    Canalrch(cell, mon) / csize / DaysInMonth(mon)
            END IF
            WRITE (50, 152) Cell, ROZone(cell), row, col, iyear, mon, DaysInMonth(mon), SF, &
                                IrrPrecRO(cell, mon), LPM(ROZone(cell)), MiToGauge(cell), SF / DaysInMonth(mon)
        END IF
        
        IF (IrrPrecRO(cell, mon) .GT. 0.) THEN
            WRITE (60, 162) Cell, iyear, mon, IrrPrecRO(Cell, mon), RO2DP, SF, RO2ET
        END IF
        
    END DO

101 FORMAT(I6,',', I5,',', F6.2)
132	FORMAT(I6,',', I4,',', 2(I3,','), I4,',', 2(I2,','), 3(F7.2,','), F4.2,',',F6.2,',',F5.2,',', F14.10,',',F14.10)
142 FORMAT(I6,', ',2(I3,', '),I4,', ',2(I2,', '),F16.10,', ',F14.12)
152 FORMAT(I6,',', I4,',', 2(I3,','), I4,',', 2(I2,','), F4.2,',',F8.2,',',F4.2,',',F6.2,',',F14.10)
162 FORMAT(I6,',',I4,',', I2, ',',3(F8.2,','),F8.2)
172 FORMAT(I6, ', ', I4, ', ', I2, 8(', ', F8.2))  
173 FORMAT(I6, ', ', I4, ',  0', 8(', ', F8.2))  

    end associate
END SUBROUTINE popcellrch
!*****************************************************************************