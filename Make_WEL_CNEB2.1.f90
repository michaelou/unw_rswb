!********************************************************************************
!
!  Make_WEL  - Program creates .WEL file for input into ground water model
!
!  Initial Development:  Feb 2008
!	By:  Marc Groff - TFG
!
!	  Revised:	5 Oct 2010
!	  Subject:	Revised to work with new DNR dataset and 1/2 mile input grid
!
!     Revised:  October 2011
!     Subject:  Converted to monthly values - IM
!********************************************************************************
!   
!   ActiveCellCol           The column location of a given cell (cell)
!   ActiveCellRow           The row location of a given cell (cell)
!   Cell                    The cell being simulated
!   CellMI                  Monthly Municipal and Industrial pumping (month)
!   CellPump                Monthly Irrigation pumping values (month)
!   DaysInMonth             The number days in a given month (month)
!   EndYr                   The last year being simulated
!   Header                  Dummy varialbe to read header rows
!   INDIR                   The directory location of the input files
!   IrrCellPumpAF           Irrigation pumping values (cell, year, month)
!   IrrPumpYr               Year value holder for irrigation pumping
!   LinesPerStressPeriod    Number of months that have pumping values in a cell (Year, Month)
!   MuniCellPumpAF          Municipal and Industrial pumping values (cell, year, month)
!   ncell                   number of cells being simulated
!   ncol                    number of columns in the grid
!   nrow                    number of rows in the grid
!   OUTDIR                  The directory location of the output files
!   ProcessMo               Month counter
!   ProcessYr               Year counter
!   StartYr                 The first year being simulated
!   SwcYr                   The year where the results switch from monthly to annual
!
!********************************************************************************


!********************************************************************************
subroutine UNW_Make_WEL

    USE PARAM
    IMPLICIT NONE
    
  
    CHARACTER*150   OUTDIR2    
    character(len=4)  :: mYRT
    integer           :: col, row
    
    WRITE (output_unit, '(A)') 'MakeWEL'
    
    call openoutfile (10, TRIM(OUTDIR), 'Pmp_values.txt')
    WRITE (10, 100)
    
    if (.not. allocated(pctL1)) call errexit('must define PLAYER before MakeWEL')
    
    DO iyear = startyr, endyr
        WRITE(output_unit, '(I6, I8)') iyear, cyr(iyear)
        WRITE(YRT, '(I4)') iyear
        
        IF (MOD(iyear, 4) .EQ. 0) THEN
            DaysInMonth(2) = 29
            DIY = 366
        ELSE
            DaysInMonth(2) = 28
            DIY = 365
        END IF
        
        startmo = 1
        endmo = 12
        
        IrrCellp = 0.
        !Read in Annual Irrigation Pumping from WSPP
        OPEN (1, FILE = TRIM(OUTDIR)//'pump\pump'//TRIM(YRT)//'.txt', STATUS = 'OLD')
        READ (1, *)
        DO WHILE (.NOT. EOF(1))
            READ (1, *) Cell, Yr, CellPump(:)
            
            IF ((Yr .LT. StartYr) .OR. (Yr .GT. Endyr)) GOTO 13
            IrrCellp (Cell, :) = CellPump(:)
13      END DO
        CLOSE (1)
        
        MuniCellp = 0.
        IF (Muni .EQ. 1) THEN
            OPEN (13, FILE = TRIM(MIDIR), STATUS = 'OLD')
            DO WHILE (.NOT. EOF(13))
                READ (13, *) MIfold, MIFile
                
                OPEN (14, FILE = TRIM(MIfold)//'\'//TRIM(MIFile)//TRIM(YRT)//'.txt', STATUS = 'OLD')
                READ (14, *)
                DO WHILE (.NOT. EOF(14))
                    READ (14, *) cell, yr, pvol(:)
                    MuniCellp(cell, :) = MuniCellp(cell, :) + pvol(:)
                END DO
                Close(14)
                
            END DO
            CLOSE(13)
        END IF
        
        MiscCellp = 0.
        IF (MISP .EQ. 1) THEN
        !Read in Annual Miscellaneou Pumping
            OPEN (13, FILE = TRIM(MiscDIR), STATUS = 'OLD')
            DO WHILE (.NOT. EOF(13))
                READ (13, *) MISCfold, MISCfile
            
                OPEN (14, FILE = TRIM(MISCfold)//'\'//TRIM(MISCfile)//TRIM(YRT)//'.txt', STATUS = 'OLD')
                READ (14, *)
                DO WHILE (.NOT. EOF(14))
                    READ (14, *) cell, yr, pvol(:)
                    MiscCellp(Cell, :) = pvol(:)
                END DO
                CLOSE (14)
            END DO
            CLOSE(13)
        END IF
        
        LPSP = 0
        DO i = 1, ncell
            DO n = 1, 12
                IF (iyear .LT. SwcYr) THEN
                    IF (IrrCellp(i, n) + MiscCellp(i, n) + MuniCellp(i, n) .GT. 0.) THEN
                        IF ((pctL1(i) .GT. 0.) .AND. (pctL1(i) .LT. 1.)) THEN
                            LPSP(1) = LPSP(1) + 2
                        ELSE 
                            LPSP(1) = LPSP(1) + 1
                        END IF
                        GOTO 14
                    END IF
                ELSE
                    IF (IrrCellp(i, n) + MiscCellp(i, n) + MuniCellp(i, n) .GT. 0.) THEN
                        IF ((pctL1(i) .GT. 0.) .AND. (pctL1(i) .LT. 1.)) THEN
                            LPSP(n) = LPSP(n) + 2
                        ELSE
                            LPSP(n) = LPSP(n) + 1
                        END IF
                    END IF
                END IF
            END DO
14      END DO
        
        MLPSP = MAXVAL(LPSP)
        
        call openoutfile(20, TRIM(OUTDIR)//'WEL', TRIM(YRT)//'.WEL')
        WRITE (20, 120) MLPSP
        
        call openoutfile(30, TRIM(OUTDIR)//'Well_chk', 'Well_chk'//TRIM(YRT)//'.txt')
        WRITE (30, 130)
        
        IF (iyear .LT. SwcYr) THEN
            WRITE (20, 120) LPSP(1)
            DO i = 1, ncell
                col = MOD(i, ncols)
                IF (col .EQ. 0) col = ncols
                row = (i - col) / ncols + 1
                
                Totp = SUM(IrrCellp(i, 1 : 12)) + SUM(MiscCellp(i, 1 : 12)) + SUM(MuniCellp(i, 1 : 12))
                WRITE (10, 101) i, iyear, pctL1(i), MAX(Totp, 0.) / csize * 12
                
                IF (Totp .GT. 0.) THEN
                    IF (pctL1(i) .EQ. 1.) THEN
                        Layer = 1
                        WRITE (20, 121) Layer, row, col, 0.0 - (Totp * 43560 / DIY)
                        WRITE (30, 131) i, Layer, source(i), row, col, iyear, MISP * 0, DIY, Totp / DIY, Totp * 43560 / DIY
                    ELSE IF (pctL1(i) .EQ. 0.) THEN
                        Layer = 2   
                        WRITE (20, 121) Layer, row, col, 0.0 - (Totp * 43560 / DIY)
                        WRITE (30, 131) i, Layer, source(i), row, col, iyear, MISP * 0, DIY, Totp / DIY, Totp * 43560 / DIY
                    ELSE
                        Layer = 1
                        WRITE (20, 121) Layer, row, col, 0.0 - (Totp * pctL1(i) * 43560 / DIY)
                        WRITE (30, 131) i, Layer, source(i), row, col, iyear, MISP * 0, DIY, Totp * pctL1(i) / DIY, Totp * pctL1(i) * 43560 / DIY
                        
                        Layer = 2
                        WRITE (20, 121) Layer, row, col, 0.0 - (Totp * (1 - pctL1(i)) * 43560 / DIY)
                        WRITE (30, 131) i, Layer, source(i), row, col, iyear, MISP * 0, DIY, Totp * (1 - pctL1(i)) / DIY, Totp * (1 - pctL1(i)) * 43560 / DIY
                    END IF
                    
                END IF
            END DO
        ELSE
            DO n = StartMo, EndMo
            
                WRITE (20, 120) LPSP(n)
                DO i = 1, ncell
                
                    col = MOD(i, ncols)
                    IF (col .EQ. 0) col = ncols
                    row = (i - col) / ncols + 1
                
                    Totp = IrrCellp(i, n) + MiscCellp(i, n) + MuniCellp(i, n)
                    
                    IF (Totp * 43560 / DaysInMonth(n) .GT. 1000000.) THEN
                        CONTINUE
                    END IF
                
                    IF (Totp .GT. 0.) THEN
                        IF (pctL1(i) .GE. 1.) THEN
                            Layer = 1
                            WRITE (20, 121) Layer, row, col, 0.0 - (Totp * 43560 / DaysInMonth(n))
                            WRITE (30, 131) i, Layer, source(i), row, col, iyear, n, DaysInMonth(n), Totp / DaysInMonth(n), Totp * 43560 / DaysInMonth(n)
                        ELSE IF (pctL1(i) .LE. 0.) THEN
                            Layer = 2
                            WRITE (20, 121) Layer, row, col, 0.0 - (Totp * 43560 / DaysInMonth(n))
                            WRITE (30, 131) i, Layer, source(i), row, col, iyear, n, DaysInMonth(n), Totp / DaysInMonth(n), Totp * 43560 / DaysInMonth(n)
                        ELSE
                            Layer = 1
                            WRITE (20, 121) Layer, row, col, 0.0 - (Totp * pctL1(i) * 43560 / DaysInMonth(n))
                            WRITE (30, 131) i, Layer, source(i), row, col, iyear, n, DaysInMonth(n), Totp * pctL1(i) / DaysInMonth(n), Totp * pctL1(i) * 43560 / DaysInMonth(n)
                            
                            Layer = 2
                            WRITE (20, 121) Layer, row, col, 0.0 - (Totp * (1 - pctL1(i)) * 43560 / DaysInMonth(n))
                            WRITE (30, 131) i, Layer, source(i), row, col, iyear, n, DaysInMonth(n), Totp * (1 - pctL1(i)) / DaysInMonth(n), Totp * (1 - pctL1(i)) * 43560 / DaysInMonth(n)
                        END iF
                    END IF
                    
                    IF (n .EQ. 1) THEN
                        TPy = SUM(IrrCellp(i, 1 : 12)) + SUM(MiscCellp(i, 1 : 12)) + SUM(MuniCellp(i, 1 : 12))
                        WRITE (10, 101) i, iyear, pctL1(i), MAX(TPy, 0.) / csize * 12
                    END IF
                
                END DO
            END DO
        END IF
        
        CLOSE (20)
        CLOSE (30)
        
    END DO

!199 FORMAT('# MODFLOW 2000 Well Package')
!200 FORMAT('PARAMETER  0  0')
!201 FORMAT(2I10) !Header for WEL file (File 20)
120 FORMAT(2I10)	!Lines in the stress period (File 20)
121	FORMAT(3I10, E10.4)  !Layer, Row, Col, Pumpage FT^3/Day in stress period (File 20)
130	FORMAT ('Cell, Layer, Well_Data_Source, Row, Column, Year, Month, NumberOfDaysInPeriod, Pumping_AFpD, Pumping_cfd') !Header (File 30)
131	FORMAT (I5, 2(', ', I1), ', ', 2(I3, ', '), I4, ', ', 2(I3, ', '), F12.7, ', ', F12.2) !Output for WEL_Check.txt (File 30)
100 FORMAT ('Cell, Year, Pumping_AF')
101 FORMAT (I6, ',', I5, 2(',', F6.2))

!    WRITE(6,*) 'Program Successfully Completed'

END subroutine UNW_Make_WEL
!*********************************************************************************
