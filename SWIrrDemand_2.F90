!********************************************************************************
!
!  UNW_SWIrrDemands  - Program computes demands from Surface Water Irrigation for
!		    			the STELLA portion of the 
!			    		Cohyst Conjunctive Management Model
!
!  Initial Development:  Feb 2008
!	By:  Marc Groff - TFG
!
!  Modifications:
!	June 2008 - Modified by TFG to reflect updated acreage file inputs through 2005	
!	Sept 2010 - Modified by TFG to reflect new structure of landuse, soils, and 
!				precip input files necessitated by 1/2 mile grid cell 
!
!  Further Modification:
!   Sept 2011 - Modified by TFG to make calculations on a cell by cell basis, by 
!               removing the curve coefficients from the program and reorgainizing
!               the model. - IM
!
!   Febr 2012 - Modified by TFG to include 12 different crops.  Further Adjustments were 
!               made to reduce the code modifications necessary to run the program.
!
!   July 2012 - Revamped by TFG to make the process run faster based on an annual files.
!
!********************************************************************************
!********************************************************************************

!***********************************************************
subroutine UNW_SWIRRDEMAND
    use Param
    IMPLICIT NONE
    
    WRITE (output_unit, '(A)') 'IrrDemand'
    
    !POPULATE CELLS WITH LOCATION VARIALBES
    if (.not. allocated(CoefZone)) call errexit('must define CELLLOC before IrrDemand')
    
    !LOAD CELL COEFFICIENTS
    if (.not. allocated(DryETadj)) call errexit('must define COEFF before IrrDemand')
    
    !Load Application Efficiencies
    if (.not. allocated(AEg)) call errexit('must define EFFI before IrrDemand')
    
    !Load Call Year Identification
    if (all(cyr == imiss)) call errexit('must define YEARS or ACTYR before IrrDemand')

    DO iyear = startyr, endyr
        
        WRITE(output_unit, '(I6, I8)') iyear, cyr(iyear)
        WRITE(YRT, '(I4)') iyear
        WRITE(YRTc, '(I4)') cyr(iyear)
        
        CALL OpenCSFiles1
        CALL ReadCSFiles1
        
        OPEN (1, FILE = TRIM(LUDIR)//TRIM(LandUse)//TRIM(YRT)//'.txt', STATUS = 'OLD')
        READ (1, *)
        
        
        call openoutfile(10, TRIM(OUTDIR)//trim(SUBOUT)//'/'//'SWD', 'SWD'//TRIM(YRT)//'.txt')        
        call openoutfile(11, TRIM(OUTDIR)//trim(SUBOUT)//'/'//'GWP', 'GWP'//TRIM(YRT)//'.txt')        
        call openoutfile(12, TRIM(OUTDIR)//trim(SUBOUT)//'/'//'COD', 'COD'//TRIM(YRT)//'.txt')
        call openoutfile(13, TRIM(OUTDIR)//trim(SUBOUT)//'/'//'COP', 'COP'//TRIM(YRT)//'.txt')
        
        WRITE (10, 100)
        WRITE (11, 100)
        WRITE (12, 100)
        WRITE (13, 100)
            
        
        DO WHILE (.NOT. EOF(1))
            READ (1, 201) Cell, Yr, DryAc, GWAc, SWAc, COAc
            AEgw = AEg(iyear) * AEgadj(Coefzone(cell))
            AEsw = AEs(iyear) * AEsadj(Coefzone(cell))
            
            IF (SUM(GWAc) + SUM(SWAc) + SUM(COAc) .EQ. 0.) GOTO 2
            SWD = 0.
            COD = 0.
            GWP = 0.
            COP = 0.
            SWDCell = 0.
            GWPCell = 0.
            CODCell = 0.
            COPCell = 0.
                        
            DO j = 1, 12
                NIR = MAX(NIRCSC(Cell, j, :), 0.)
                IF (SWAc(j) .GT. 0.) THEN
                    SWD(j, :) = NIR * NIRadjFactor(coefzone(cell)) / (12 * AEsw)
                    SWD(j, 0) = SUM(SWD(j, 1 : 12))
                    SWDCell(1 : 12) = SWDCell(1 : 12) + SWD(j, 1 : 12) * SWAc(j)
                END IF
                
                IF (COAc(j) .GT. 0.) THEN
                    COD(j, :) = NIR * CMsplit(coefzone(cell)) * NIRadjFactor(coefzone(cell)) / (12 * AEsw)
                    COD(j, 0) = SUM(COD(j, 1: 12))
                    CODCell(1 : 12) = CODCell(1 : 12) + COD(j, 1 : 12) * COAc(j)
                    
                    COP(j, :) = NIR * (1 - CMsplit(coefzone(cell))) * NIRadjFactor(coefzone(cell)) / (12 * AEgw)
                    COP(j, 0) = SUM(COP(j, 1: 12))
                    COPCell(1 : 12) = COPCell(1 : 12) + COP(j, 1 : 12) * COAc(j)
                END IF
                
                IF (GWAc(j) .GT. 0.) THEN
                    GWP(j, :) = NIR * NIRadjFActor(coefzone(cell)) / (12 * AEgw)
                    GWP(j, 0) = SUM(GWP(j, 1: 12))
                    GWPCell(1 : 12) = GWPCell(1 : 12) + GWP(j, 1 : 12) * GWAc(j)
                END IF
            END DO
            
            SWDCell(0) = SUM(SWDCell(1 : 12))
            CODCell(0) = SUM(CODCell(1 : 12))
            GWPCell(0) = SUM(GWPCell(1 : 12))
            
            IF (SWDCell(0) .GT. 0.) WRITE (10, 900) Cell, Yr, SWDCell(:), (SWD(i, :), i = 1, 12)
            IF (GWPCell(0) .GT. 0.) WRITE (11, 900) Cell, Yr, GWPCell(:), (GWP(i, :), i = 1, 12)
            IF (CODCell(0) .GT. 0.) THEN
                WRITE (12, 900) Cell, Yr, CODCell(:), (COD(i, :), i = 1, 12)
                WRITE (13, 900) Cell, Yr, COPCell(:), (COP(i, :), i = 1, 12)
            END IF
            
2       END DO
        
        CLOSE (1)
        CLOSE (10)
        CLOSE (11)
        CLOSE (12)
        CLOSE (13)
        CALL CloseCSFiles1
        
    END DO
    
100 FORMAT ('Cell, Year, ' &
                'Ctot-Ann, Ctot-Jan, Ctot-Feb, Ctot-Mar, Ctot-Apr, Ctot-May, Ctot-Jun, Ctot-Jul, Ctot-Aug, Ctot-Sep, Ctot-Oct, Ctot-Nov, Ctot-Dec, ' & 
                'Corn-Ann, Corn-Jan, Corn-Feb, Corn-Mar, Corn-Apr, Corn-May, Corn-Jun, Corn-Jul, Corn-Aug, Corn-Sep, Corn-Oct, Corn-Nov, Corn-Dec, ' &
                'Beet-Ann, Beet-Jan, Beet-Feb, Beet-Mar, Beet-Apr, Beet-May, Beet-Jun, Beet-Jul, Beet-Aug, Beet-Sep, Beet-Oct, Beet-Nov, Beet-Dec, ' &
                'Bean-Ann, Bean-Jan, Bean-Feb, Bean-Mar, Bean-Apr, Bean-May, Bean-Jun, Bean-Jul, Bean-Aug, Bean-Sep, Bean-Oct, Bean-Nov, Bean-Dec, ' &
                'Alfa-Ann, Alfa-Jan, Alfa-Feb, Alfa-Mar, Alfa-Apr, Alfa-May, Alfa-Jun, Alfa-Jul, Alfa-Aug, Alfa-Sep, Alfa-Oct, Alfa-Nov, Alfa-Dec, ' &
                'Whet-Ann, Whet-Jan, Whet-Feb, Whet-Mar, Whet-Apr, Whet-May, Whet-Jun, Whet-Jul, Whet-Aug, Whet-Sep, Whet-Oct, Whet-Nov, Whet-Dec, ' &
                'Pota-Ann, Pota-Jan, Pota-Feb, Pota-Mar, Pota-Apr, Pota-May, Pota-Jun, Pota-Jul, Pota-Aug, Pota-Sep, Pota-Oct, Pota-Nov, Pota-Dec, ' &
                'Sorg-Ann, Sorg-Jan, Sorg-Feb, Sorg-Mar, Sorg-Apr, Sorg-May, Sorg-Jun, Sorg-Jul, Sorg-Aug, Sorg-Sep, Sorg-Oct, Sorg-Nov, Sorg-Dec, ' &
                'Sunf-Ann, Sunf-Jan, Sunf-Feb, Sunf-Mar, Sunf-Apr, Sunf-May, Sunf-Jun, Sunf-Jul, Sunf-Aug, Sunf-Sep, Sunf-Oct, Sunf-Nov, Sunf-Dec, ' &
                'Soyb-Ann, Soyb-Jan, Soyb-Feb, Soyb-Mar, Soyb-Apr, Soyb-May, Soyb-Jun, Soyb-Jul, Soyb-Aug, Soyb-Sep, Soyb-Oct, Soyb-Nov, Soyb-Dec, ' &
                'SGra-Ann, SGra-Jan, SGra-Feb, SGra-Mar, SGra-Apr, SGra-May, SGra-Jun, SGra-Jul, SGra-Aug, SGra-Sep, SGra-Oct, SGra-Nov, SGra-Dec, ' &
                'Fall-Ann, Fall-Jan, Fall-Feb, Fall-Mar, Fall-Apr, Fall-May, Fall-Jun, Fall-Jul, Fall-Aug, Fall-Sep, Fall-Oct, Fall-Nov, Fall-Dec, ' &
                'Past-Ann, Past-Jan, Past-Feb, Past-Mar, Past-Apr, Past-May, Past-Jun, Past-Jul, Past-Aug, Past-Sep, Past-Oct, Past-Nov, Past-Dec')

201 FORMAT (I6, 2X, I5, 48(2X, F6.2))
    
900 Format (I7, ",", I5, 169(",", F14.8))

END subroutine UNW_SWIRRDEMAND

!********************************************************************
SUBROUTINE OpenCSFiles1

!   The OpenCSFiles subroutine opens the individual net irrigation requirement
!   input files and reads in the header line.

	use Param, only: YRTc, WBPDIR, Crops
    IMPLICIT NONE
    
    INTEGER     nn
    
    DO nn = 1, 12
        IF (nn .NE. 11) THEN
            OPEN (620 + nn, FILE = TRIM(WBPDIR)//'NIR\'//TRIM(Crops(nn))//'-NIR'//TRIM(YRTc)//'.txt', STATUS = 'OLD')
            READ (620 + nn, *)
        END IF
    END DO
    
END SUBROUTINE OpenCSFiles1

!********************************************************************
SUBROUTINE ReadCSFiles1

!   The ReadCSFiles subroutine reads the CropSim net irrigation requirement
!   for each potential crop in a given cell.

	use Param, only: NIRCSC
    IMPLICIT NONE

	INTEGER nn, i
    INTEGER CSCell, CSyr, CScrop
	
	DO nn = 1, 12
        IF (nn .NE. 11) THEN
            DO WHILE (.NOT. EOF(620 + nn))
                READ (620 + nn, *, END = 50) CSCell, CSYr, CSCrop, (NIRCSC(CSCell, nn, i), i = 0, 12)
50          END DO
		END IF
	END DO !n

!300 Format statement based on program Grid_MONfiles_xx-TT.f90, format line 490
300	FORMAT (I6, 2X, I4, 1X, I3, 1X, F8.2, 12(X, F8.2))

END SUBROUTINE ReadCSFiles1

!********************************************************************
SUBROUTINE CloseCSFiles1
    IMPLICIT NONE
    INTEGER nn
    
    DO nn = 1, 12
        IF (nn .NE. 11) THEN
            CLOSE (620 + nn)
        END IF
    END DO
    
END SUBROUTINE CloseCSFiles1
!********************************************************************