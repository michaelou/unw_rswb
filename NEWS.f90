
program news
  use Param
  use ifport, only : chdir
  implicit none
  integer :: iostat, ifil_name
  character*1024 :: namfile, MASTER
  integer, parameter :: linelen = 2048
  integer :: nchar
  character*(linelen) :: sline
  character*(48) :: sword
  integer :: CntNum, geoid
  logical :: rIAD=.FALSE., rWSPP=.FALSE., rCWEL=.FALSE., rCRCH=.FALSE., rMWEL=.FALSE., rMRCH=.FALSE., rREPORT=.FALSE.
  
  print *, 'Start the NEWS model ... '
  
  call get_command_argument(1, namfile)
  open(newunit=ifil_name, file=trim(namfile), action='read', status='old', iostat=iostat)
  if (iostat /= 0) call errexit('could not find the name file: "'//trim(namfile)//'"'//new_line('a')// &
    '  exit the program'//new_line('a')// &
    '  Usage: news.exe name_file')
  
  
  
  do 
    call readline(ifil_name, sline, iostat=iostat)
    if (iostat < 0) exit 
    print *, 'processing line: "'//trim(sline)//'"'
    ! read the first word
    call first_word(sline, sword, nchar)
    
    select case (trim(to_upper(sword)))
      ! define project name
      case ('PROJNAME')
        proj_name = trim(adjustl(sline(nchar + 1: linelen)))
      
      ! define the starting and ending years
      case ('YEARS')
        read(sline(nchar + 1: linelen), *, iostat=iostat) startyr, endyr
        if (iostat < 0) call errexit('error reading '//trim(sline))
        allocate(cyr(startyr : endYr))
        cyr = (/ (iyr, iyr = startyr, endYr) /)
      
      ! define the starting and ending years
      case ('CELLS')
        read(sline(nchar + 1: linelen), *, iostat=iostat) nrows, ncols, csize
        if (iostat < 0) call errexit('error reading '//trim(sline))
        ncell = nrows * ncols
        
      ! define the starting and ending years
      case ('NZONE')
        read(sline(nchar + 1: linelen), *, iostat=iostat) rzones, nzones, czones
        if (iostat < 0) call errexit('error reading '//trim(sline))
        
      ! define the starting and ending years
      case ('NCROPSOIL')
        if (ncell == imiss) call errexit('must define ROWCOL before NCROPSOIL')        
        read(sline(nchar + 1: linelen), *, iostat=iostat) ncrop, nsoil
        if (iostat < 0) call errexit('error reading '//trim(sline))
        
        
      ! define the starting and ending years
      case ('CROP')
        if (ncrop == imiss) call errexit('must define NCROPSOIL before CROP') 
        allocate(Crops(ncrop))
        read(sline(nchar + 1: linelen), *, iostat=iostat) Crops
        if (iostat < 0) call errexit('error reading '//trim(sline))
        
      ! define the soil types
      case ('SOIL')
        if (nsoil == imiss) call errexit('must define NCROPSOIL before CROP') 
        allocate(Soils(nsoil))
        read(sline(nchar + 1: linelen), *, iostat=iostat) Soils
        
      ! define the master directory
      case ('MASTER')
        MASTER = trim(adjustl(sline(nchar + 1: linelen)))
        if (chdir(trim(MASTER)) /= 0) call errexit('could not find the master directory: "'//trim(MASTER)//'"') 
        
      ! define the master directory
      case ('LUDIR')
        LUDIR = trim(adjustl(sline(nchar + 1: linelen)))
        nchar = len(trim(LUDIR))
        if (LUDIR(nchar:nchar) /= '\' .and. LUDIR(nchar:nchar) /= '/') LUDIR = trim(LUDIR)//'/'
        
      ! define the master directory
      case ('PREFIXLU')
        LandUse = trim(adjustl(sline(nchar + 1: linelen)))
        
      ! define the master directory
      case ('WBPDIR')
        WBPDIR = trim(adjustl(sline(nchar + 1: linelen)))
        nchar = len(trim(WBPDIR))
        if (WBPDIR(nchar:nchar) /= '\' .and. WBPDIR(nchar:nchar) /= '/') WBPDIR = trim(WBPDIR)//'/'
        
      ! define the master directory
      case ('OUTDIR')
        OUTDIR = trim(adjustl(sline(nchar + 1: linelen)))
        nchar = len(trim(OUTDIR))
        if (OUTDIR(nchar:nchar) /= '\' .and. OUTDIR(nchar:nchar) /= '/') OUTDIR = trim(OUTDIR)//'/'
        
      ! read the cell location file  
      case ('CELLLOC')
        if (ncell == imiss) call errexit('must define ROWCOL before CELLLOC')  
        allocate(CoefZone(ncell), ROZone(ncell), MitoGauge(ncell), cellSoilIndex(ncell))
        !WRITE (output_unit,*) 'READING CELL LOCATION'
        OPEN (20, FILE = trim(adjustl(sline(nchar + 1: linelen))), STATUS='OLD')
        READ(20, *)
        DO 
          READ (20, *, iostat=iostat) cell, cellSoilIndex(cell), CoefZone(cell), ROZone(cell), MiToGauge(cell)
          if (iostat < 0) exit
        END DO
        CLOSE (20)
      
      ! read the cell location file  
      case ('COEFF')
        if (nzones == imiss) call errexit('must define NZONE before COEFF') 
        allocate(DryETadj(nzones), IrrETadj(nzones), NIRadjFactor(nzones), AEgadj(nzones), Fslgw(nzones))
        allocate(DryETtoRO(nzones), AEsadj(nzones), Fslsw(nzones), PertoRchg(nzones), DPadj(nzones), ROadj(nzones), CMsplit(nzones))        
        allocate(DPLL(nzones), DPcap(nzones), ROfDP(nzones))
        !WRITE(output_unit, *) 'Reading Coefficient File Information'
        OPEN (11, FILE = trim(adjustl(sline(nchar + 1: linelen))), STATUS='UNKNOWN')
        READ (11, *)
        DO
          READ (11, *, iostat=iostat) m, DryETAdj(m), IrrETadj(m), NIRadjFactor(m), AEgadj(m), Fslgw(m), &
            DryETtoRO(m), AEsadj(m), Fslsw(m), PertoRchg(m), DPadj(m), ROadj(m), CMsplit(m), DPLL(m), DPcap(m), ROfDP(m)
            if (iostat < 0) exit
        END DO 
        CLOSE (11)
      
      ! read the cell location file  
      case ('COEFFRO')
        if (nzones == imiss) call errexit('must define NZONE before COEFF')  
        allocate(LPM(0:rzones))
        !WRITE(output_unit, *) 'Reading Coefficient File Information'
        OPEN(9, File = trim(adjustl(sline(nchar + 1: linelen))), STATUS = 'OLD')
        DO
          READ (9, *, iostat=iostat) Zone, LPM(Zone)
          if (iostat < 0) exit
        END DO  
        Close (9)
          
      ! read the cell location file  
      case ('EFFI')
        if (startyr == imiss) call errexit('must define YEARS before EFFI')  
        allocate(AEs(startyr : endyr), AEg(startyr : endyr))
        !WRITE (output_unit, *) 'Reading Application Efficiencies'
        OPEN (11, FILE = trim(adjustl(sline(nchar + 1: linelen))), STATUS = 'OLD')
        READ (11, *)
        DO
          READ (11, *, iostat=iostat) yr, AEsw, AEgw
          if (iostat < 0) exit
          IF (yr .LT. startyr) cycle
          IF (yr .GT. endyr) cycle
          AEg(yr) = AEgw
          AEs(yr) = AEsw          
        END DO
        !print *, AEg
        CLOSE (11)
      
      ! read the cell location file  
      case ('ACTYR')
        if (startyr == imiss) call errexit('must define YEARS before ACTYR')  
        !WRITE (6, *) 'Get WBP relationship years'
        OPEN (11, FILE = trim(adjustl(sline(nchar + 1: linelen))), STATUS = 'OLD')
        READ (11, *)
        DO 
          READ (11, *, iostat=iostat) yr, iyr
          if (iostat < 0) exit
          IF ((yr .LT. startyr) .OR. (yr .GT. endyr)) cycle
          cyr(yr) = iyr
        END DO
        CLOSE (11)
      
      ! minimum and maximum runoff
      case ('RUNOFF')
        read(sline(nchar + 1: linelen), *, iostat=iostat) minRO, maxRO
        if (maxRO <= minRO) call errexit('maximum runoff is smaller than minimum runoff')
      
      ! dryland only
      case ('DRYLAND')
        DPonly = 1
      
      ! surface water only
      case ('SURFWAT')
        SWonly = 1
      
      ! define the master directory
      case ('SUBOUT')
        SUBOUT = trim(adjustl(sline(nchar + 1: linelen)))
        
      ! define the master directory
      case ('CNLRCH')
        CnlRch = 1
        CNLDIR = trim(adjustl(sline(nchar + 1: linelen)))
        !nchar = len(trim(CNLDIR))
        !if (CNLDIR(nchar:nchar) /= '\' .and. CNLDIR(nchar:nchar) /= '/') CNLDIR = trim(CNLDIR)//'/'
      
      ! define the master directory
      case ('MISCRCH')
        MscRch = 1
        MISCDIR = trim(adjustl(sline(nchar + 1: linelen)))
      
      ! define the master directory
      case ('SWCYR')
        read(sline(nchar + 1: linelen), *, iostat=iostat) SwcYr
        if (iostat < 0) call errexit('error reading '//trim(sline))
      
      ! define the master directory
      case ('PLAYER')
        allocate(source(ncell), pctL1(ncell))
        OPEN (1, FILE = trim(adjustl(sline(nchar + 1: linelen))), STATUS = 'OLD')
        READ (1, *)
        DO 
          READ (1, *, iostat=iostat) cell, pctL1(cell), source(cell)
          if (iostat < 0) exit
        END DO
        CLOSE (1)
      
      ! define the master directory
      case ('MISCPMP')
        MUNI = 1
        MISCDIR = trim(adjustl(sline(nchar + 1: linelen)))
              
      
      ! define the master directory
      case ('MIPUMP')
        MISP = 1
        MIDIR = trim(adjustl(sline(nchar + 1: linelen)))
        
      ! 
      
      ! read the zone ID file for WSPP_Report, reads ZoneName, ZoneID
      case ('ZONEID')
        !WRITE (6, *) 'Populating County ID Numbers (FIP)'
        if (czones == imiss) call errexit('must define NZONE before ZONEID')  
        !print *, 'czones = ', czones
        allocate(ActCounty(0:czones), CountyName(0:czones), cState(0:czones))
        ActCounty = 0
        OPEN (1, FILE = trim(adjustl(sline(nchar + 1: linelen))), STATUS = 'OLD')
        READ (1, *)
        DO
          READ (1, *, iostat=iostat) CntNum, geoid, County, cState(CntNum)
          if (iostat < 0) exit
          !print *, County, CntNum
          if (CntNum > czones) call errexit('Zone ID'//trim(adjustl(sline(nchar + 1: linelen)))//' is larger than the total number of zones')  
          CountyName(CntNum) = TRIM(County)
        END DO
        CLOSE (1)
        
      ! read the cell location file  
      case ('ZONECELL')
        !WRITE(output_unit, *) 'Reading Coefficient File Information'
        if (.not. allocated(ActCounty)) call errexit('must define ZONEID before ZONECELL')  
        allocate(cntyidx(ncell))
        OPEN (1, FILE = trim(adjustl(sline(nchar + 1: linelen))), STATUS='OLD')
        READ (1, *)
        DO 
          READ (1, *, iostat=iostat) Cell, geoid, Cntnum
          if (iostat < 0) exit
          if (CntNum > czones) call errexit('Zone ID'//trim(adjustl(sline(nchar + 1: linelen)))//' is larger than the total number of zones')  
          cntyidx (Cell) = Cntnum
          ActCounty (cntnum) = 1 ! check if this zone is defined in the zone file
        END DO
        CLOSE (1)
        

          
      ! running command
      case ('IAD')
        rIAD = .TRUE.
        
      ! running command
      case ('WSPP')
        rWSPP = .TRUE.!     
          
      ! running command
      case ('MAKEWEL')
        rMWEL = .TRUE.!   
        
      ! running command
      case ('COMPWEL')
        rCWEL = .TRUE.! 
        !rchfile = trim(adjustl(sline(nchar + 1: linelen)))
          
      ! running command
      case ('MAKERCH')
        rMRCH = .TRUE.!   
        
      ! running command
      case ('COMPRCH')
        rCRCH = .TRUE.! 
        !rchfile = trim(adjustl(sline(nchar + 1: linelen)))
      
      ! running command
      case ('REPORT')
        rREPORT = .TRUE.! 
        !rchfile = trim(adjustl(sline(nchar + 1: linelen)))
        
      case default
        print *, 'Unrecognized command. It will be skipped.'	  
    end select
  end do 
  
  WRITE(output_unit, *) new_line('a')//'Allocate space ...'
  if (rIAD .or. rWSPP .or. rMRCH) then
    allocate(DryAc(ncrop), GWAc(ncrop), SWAc(ncrop), COAc(ncrop))
    allocate(NIRCSC(ncell, ncrop, 0 : nmonth_in_year), NIR(0 : nmonth_in_year)) !
    allocate(SWD(ncrop, 0 : nmonth_in_year), COD(ncrop, 0 : nmonth_in_year), GWP(ncrop, 0 : nmonth_in_year), COP(ncrop, 0 : nmonth_in_year))
    allocate(SWDCell(0:nmonth_in_year), GWPCell(0:nmonth_in_year), CODCell(0:nmonth_in_year), COPCell(0:nmonth_in_year))
  end if
  
  if (rWSPP) then    
    allocate(SWDann(ncell, 0 : nmonth_in_year), SWDcrp(ncell, ncrop, 0 : nmonth_in_year))
    allocate(GWPann(ncell, 0 : nmonth_in_year), GWPcrp(ncell, ncrop, 0 : nmonth_in_year))
    allocate(CODann(ncell, 0 : nmonth_in_year), CODcrp(ncell, ncrop, 0 : nmonth_in_year))
    allocate(COPann(ncell, 0 : nmonth_in_year), COPcrp(ncell, ncrop, 0 : nmonth_in_year))
    allocate(CellRO(4, ncrop, 0 : nmonth_in_year), CellDP(4, ncrop, 0 : nmonth_in_year), CellAP(4, ncrop, 0 : nmonth_in_year))
    allocate(DryDP(ncrop, 0:nmonth_in_year), DryRO(ncrop, 0:nmonth_in_year), DryET(ncrop, 0:nmonth_in_year))
    allocate(IrrDP(ncrop, 0:nmonth_in_year), IrrRO(ncrop, 0:nmonth_in_year), IrrET(ncrop, 0:nmonth_in_year), NIRCS(ncrop, 0:nmonth_in_year))
    
    allocate(ETMaxdry(0:nmonth_in_year), ETMAXadjdry(0:nmonth_in_year), NIRmET(0:nmonth_in_year))
    allocate(RO1dry(0:nmonth_in_year), DP1dry(0:nmonth_in_year), RO2dry(0:nmonth_in_year), DP2dry(0:nmonth_in_year))
    allocate(ETTrans(0:nmonth_in_year))
    
    allocate(ET(0:nmonth_in_year), SL(0:nmonth_in_year), DAP(0:nmonth_in_year), PSL(0:nmonth_in_year), ETGain(0:nmonth_in_year), DET1(0:nmonth_in_year), DET2(0:nmonth_in_year))
    allocate(ETbase(0:nmonth_in_year), DP1Irr(0:nmonth_in_year), DP2(0:nmonth_in_year), DP3(0:nmonth_in_year), RO1Irr(0:nmonth_in_year), RO2(0:nmonth_in_year))
    allocate(RO3(0:nmonth_in_year), EWat(0:nmonth_in_year))
    
    allocate(TotCellDP(0:nmonth_in_year), TotCellRO(0:nmonth_in_year), TotCellAP(0:nmonth_in_year))
    allocate(DP2RO(0:nmonth_in_year), DPdrytot(0:nmonth_in_year), DPirrtot(0:nmonth_in_year))
  end if
  
  if (rMWEL) then
    allocate(CellPump(0:nmonth_in_year), IrrCellp(ncell, 0:nmonth_in_year), MuniCellp(ncell, 0:nmonth_in_year), MiscCellp(ncell, 0:nmonth_in_year),pvol(0:nmonth_in_year))
  end if
  
  if (rREPORT) then
    allocate(Precip(ncell,0:nmonth_in_year))
  end if
  
  WRITE(output_unit, *) new_line('a')//'Entering calculation ...'
  if (rIAD)  call UNW_SWIRRDEMAND()
  if (rWSPP) call UNW_WSPP()
  if (rMRCH) call Make_RCH
  if (rMWEL) call UNW_Make_WEL
  
  print *,  new_line('a')//'Finish calculation, start compiling ...'
  !print *, !'******************************'
  
  if (rCRCH) call Compile_RCH()
  if (rCWEL) call Compile_Wel()
  if (rREPORT) then 
    RAWDIR = trim(OUTDIR)
    OUTDIR = trim(RAWDIR)//'REPORT/'
    call WSPP_Report()
  end if 
  
  print *,  new_line('a')//'Execution completed successfully'
end program
