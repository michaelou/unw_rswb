MODULE Param
  use iso_fortran_env, only : output_unit
  integer, parameter :: imiss = -999
  real, parameter :: rmiss = -1e30
  integer, parameter :: nmonth_in_year = 12
  
  INTEGER :: startyr = imiss
  INTEGER :: endyr 
  INTEGER :: nzones = imiss
  INTEGER :: rzones
  INTEGER :: czones
  INTEGER :: ncell = imiss
  INTEGER :: nrows
  INTEGER :: ncols
  INTEGER :: ncrop = imiss
  INTEGER :: nsoil = imiss
  
  CHARACTER*50    proj_name
  CHARACTER*1024  :: LUDIR='', OUTDIR='', INDIR='', WBPDIR='', CNLDIR='', RAWDIR='', SUBOUT=''
  CHARACTER*4     YRT, cYRT, YRTc
  CHARACTER*12, allocatable  :: Crops(:), Soils(:)
  INTEGER, allocatable :: cellCropIndex(:), cellSoilIndex(:)
  CHARACTER*12 :: LandUse
  INTEGER, allocatable :: cyr(:), CoefZone(:), ROZone(:)
  real, allocatable ::  MiToGauge(:)
  INTEGER     iyear, yr, iyr, CalcYr, pflag
  INTEGER     cell, zone
  INTEGER     h, i, j, k, m, n, l
  INTEGER     CSCell, CSYear, CSCrop
  
  REAL, allocatable :: DryETadj(:), IrrETadj(:), NIRadjFactor(:), AEgadj(:), Fslgw(:)
  REAL, allocatable :: DryETtoRO(:), AEsadj(:), Fslsw(:), PertoRchg(:), DPadj(:), ROadj(:), CMsplit(:)
  REAL, allocatable :: DryAc(:), GWAc(:), SWAc(:), COAc(:)
  REAL, allocatable :: NIRCSC(:, :, :), NIR(:)
  REAL, allocatable :: SWD(:, :), COD(:, :), GWP(:, :), COP(:, :)
  REAL, allocatable :: AEs(:), AEg(:)
  REAL, allocatable :: SWDCell (:), GWPCell(:), CODCell(:), COPCell(:)
  REAL, allocatable :: DPLL(:), DPcap(:), ROfDP(:)
  REAL, allocatable :: DP2RO(:), DPdrytot(:), DPirrtot(:), ETTrans(:)
  REAL :: AEsw, AEgw 
  
  real :: minRO=0., maxRO=0.
  
  !0 - irrigation included; 1 - dryland only
  integer :: DPonly = 0
  
  !0 - all irrigation; 1 - no GWP or COP
  integer :: SWonly = 0
  
  !0 - use irrigation demand; 1 use irrigation applied
  integer :: MeterPump = 0
  
  INTEGER, allocatable :: CropCode(:)
  REAL, allocatable :: SWDann(:, :), SWDcrp(:, :, :)
  REAL, allocatable :: GWPann(:, :), GWPcrp(:, :, :)
  REAL, allocatable :: CODann(:, :), CODcrp(:, :, :)
  REAL, allocatable :: COPann(:, :), COPcrp(:, :, :)
  
  

  REAL, allocatable :: CellRO(:, :, :), CellDP(:, :, :), CellAP(:, :, :)
  REAL    CellET, CellSL, CellSW, CellDAP, CellPSL, CellETGain, CellDET, CellETbase
  REAL    CellDP1, CellDP2, CellDP3, CellRO1, CellRO2, CellRO3, CellEWAT, CellNIRmET
  
  REAL, allocatable :: DryDP(:, :), DryRO(:, :), DryET(:, :)
  REAL, allocatable :: IrrDP(:, :), IrrRO(:, :), IrrET(:, :), NIRCS(:, :)
  
  REAL, allocatable :: ETMaxdry(:), ETMAXadjdry(:), NIRmET(:)
  REAL, allocatable :: RO1dry(:), DP1dry(:), RO2dry(:), DP2dry(:)
  real :: CellETTrans
  
  REAL, allocatable :: ET(:), SL(:), DAP(:), PSL(:), ETGain(:), DET1(:), DET2(:)
  REAL, allocatable :: ETbase(:), DP1Irr(:), DP2(:), DP3(:), RO1Irr(:), RO2(:)
  REAL, allocatable :: RO3(:), EWat(:)
  
  REAL, allocatable :: TotCellDP(:), TotCellRO(:), TotCellAP(:)
  
  real :: csize=0    
  
  
  ! MAKE WELL RCH
  REAL, allocatable :: LPM(:)
  integer :: CnlRch=0, MscRch=0
  
  !real        :: DaysInMonth(nmonth_in_year) = (/30.4, 30.4, 30.4, 30.4, 30.4, 30.4, 30.4, 30.4, 30.4, 30.4, 30.4, 30.4/) ! CNEB
  integer     :: DaysInMonth(nmonth_in_year) = (/31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31/)                         ! UNW
  integer     :: layer
  INTEGER     :: startmo, endmo
  INTEGER     :: LPSP(nmonth_in_year), MLPSP
  INTEGER     :: MISP=0, MUNI=0, DIY, SwcYr=0
  CHARACTER*20    MIfold, MIfile, MISCfold, MISCfile
  CHARACTER*1024  :: MIDIR='', MISCDIR='', pmpfile
  REAL, allocatable :: CellPump(:), IrrCellp(:, :), MuniCellp(:, :), MiscCellp(:, :)
  REAL, allocatable :: pvol(:)
  real :: Totp, TPy
  INTEGER, allocatable :: source(:)    
  REAL, allocatable :: pctL1(:)
  
  ! WSPP_Report
  
  INTEGER, allocatable :: cntyidx(:), ActCounty(:)
  CHARACTER*25 ::  County
  !CHARACTER*25, allocatable  ::  nCounty(:)
  CHARACTER*25,     allocatable, dimension(:)       :: CountyName, cState
  REAL, allocatable ::    Precip(:, :)
  integer,     parameter :: nIrrLandType = 4
  CHARACTER*3, parameter :: Irrig(nIrrLandType) = (/'Dry', 'GW ', 'SW ', 'CO '/)
  
contains
  
  subroutine readline(ifile, out_string, iostat)
    implicit none
    integer :: ifile, iostat
    character(len=*) :: out_string
    
    do
      read(ifile, '(A)', iostat=iostat) out_string
      if (iostat < 0) return
      if (out_string(1:1) /= '#' .and. out_string /= '') return
    end do
  end subroutine
  
  function to_upper(strIn) result(strOut)
    ! Adapted from http://www.star.le.ac.uk/~cgp/fortran.html (25 May 2012)
    ! Original author: Clive Page
    
    implicit none
    
    character(len=*), intent(in) :: strIn
    character(len=len(strIn)) :: strOut
    integer :: i,j
    
    do i = 1, len(strIn)
      j = iachar(strIn(i:i))
      if (j>= iachar("a") .and. j<=iachar("z") ) then
        strOut(i:i) = achar(iachar(strIn(i:i))-32)
      else
        strOut(i:i) = strIn(i:i)
      end if
    end do
  end function to_upper
  
  
  subroutine first_word(sline, sword, nchar)
    implicit none
    character(len=*) :: sline, sword
    integer :: nchar
    
    read(sline, *) sword
    nchar = len(trim(sword))
  end subroutine
  
  subroutine errexit(s)
    USE ISO_FORTRAN_ENV, ONLY : ERROR_UNIT ! access computing environment
    character*(*) :: s
    write(ERROR_UNIT, *) s
    
    stop
    
  end subroutine
  
  subroutine openoutfile(ifile, fdir, fname)
    USE IFPORT, only: MAKEDIRQQ 
    integer :: ifile
    character(len=*) :: fdir, fname
    integer :: ios
    
    integer :: l
    character(len=len_trim(fdir)+1) :: dir
    !print *, fdir
    !print *, fname
    l = len_trim(fdir)
    if (fdir(l:l) /= '/' .and. fdir(l:l) /= '\') then; dir=trim(fdir)//'/'; else; dir=fdir; endif
    openfile: do
      OPEN (ifile, File=trim(dir)//trim(fname), STATUS='replace', iostat=ios)
      if (ios /= 0) then
        ! create directory
        !print *, 'mkdir '//trim(fdir)
        call system('mkdir '//trim(Replace_slash(dir))//' > nul 2>&1')
      else
        return
        
      end if 
    end do openfile
  end subroutine  
  
  ! ------------------
  FUNCTION Replace_slash (s)  RESULT(outs)
  CHARACTER(*)        :: s
  CHARACTER(LEN(s))   :: outs     ! provide outs with extra 100 char len
  INTEGER             :: i

  outs = s
  DO
     i = INDEX(outs, '/') ; IF (i == 0) EXIT
     outs(i:i) = '\'
  END DO
  END FUNCTION

END MODULE Param


module param2
  
    ! Regional Totals
    DOUBLE PRECISION, allocatable ::    RegionPrecip(:,:)
    DOUBLE PRECISION, allocatable ::    RegionAc(:,:)
    DOUBLE PRECISION, allocatable ::    RegionAP(:,:)
    DOUBLE PRECISION, allocatable ::    RegionSW(:,:)
    DOUBLE PRECISION, allocatable ::    RegionET(:,:)
    DOUBLE PRECISION, allocatable ::    RegionRO(:,:)
    DOUBLE PRECISION, allocatable ::    RegionDP(:,:)
    DOUBLE PRECISION, allocatable ::    RegionSL(:,:)
    DOUBLE PRECISION, allocatable ::    RegionPSL(:,:)
    DOUBLE PRECISION, allocatable ::    RegionDAP(:,:)
    DOUBLE PRECISION, allocatable ::    RegionETG(:,:)
    DOUBLE PRECISION, allocatable ::    RegionDET(:,:)
    DOUBLE PRECISION, allocatable ::    RegionETB(:,:)
    DOUBLE PRECISION, allocatable ::    RegionDP1(:,:)
    DOUBLE PRECISION, allocatable ::    RegionDP2(:,:)
    DOUBLE PRECISION, allocatable ::    RegionRO1(:,:)
    DOUBLE PRECISION, allocatable ::    RegionRO2(:,:)
    DOUBLE PRECISION, allocatable ::    RegionEWAT(:,:)
    DOUBLE PRECISION, allocatable ::    RegionSF(:,:)
    DOUBLE PRECISION, allocatable ::    RegionRO2DP(:,:)
    DOUBLE PRECISION, allocatable ::    RegionRO2ET(:,:)
    DOUBLE PRECISION, allocatable ::    RegionETtrans(:,:)
    DOUBLE PRECISION, allocatable ::    RegionNIRmET(:,:)

    ! Regional Irrigation Totals
    DOUBLE PRECISION, allocatable ::    RegIrrPrecip(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegIrrAc(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegIrrAP(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegIrrSW(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegIrrET(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegIrrRO(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegIrrDP(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegIrrSL(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegIrrPSL(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegIrrDAP(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegIrrETG(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegIrrDET(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegIrrETB(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegIrrDP1(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegIrrDP2(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegIrrRO1(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegIrrRO2(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegIrrEWAT(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegIrrSF(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegIrrRO2DP(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegIrrRO2ET(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegIrrETtrans(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegIrrNIRmET(:,:,:)
    
    ! Regional Crop Totals
    DOUBLE PRECISION, allocatable ::    RegCropPrecip(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegCropAc(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegCropAP(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegCropSW(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegCropET(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegCropRO(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegCropDP(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegCropSL(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegCropPSL(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegCropDAP(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegCropETG(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegCropDET(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegCropETB(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegCropDP1(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegCropDP2(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegCropRO1(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegCropRO2(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegCropEWAT(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegCropSF(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegCropRO2DP(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegCropRO2ET(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegCropETtrans(:,:,:)
    DOUBLE PRECISION, allocatable ::    RegCropNIRmET(:,:,:)
    
    !Regional crop and irrigation Totals
    DOUBLE PRECISION, allocatable ::    RICPrecip(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    RICAc(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    RICAP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    RICSW(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    RICET(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    RICRO(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    RICDP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    RICSL(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    RICPSL(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    RICDAP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    RICETG(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    RICDET(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    RICETB(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    RICDP1(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    RICDP2(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    RICRO1(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    RICRO2(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    RICEWAT(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    RICSF(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    RICRO2DP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    RICRO2ET(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    RICETtrans(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    RICNIRmET(:,:,:,:)
     
    ! County Totals
    DOUBLE PRECISION, allocatable ::    cntyPrecip(:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyAc(:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyAP(:,:,:)
    DOUBLE PRECISION, allocatable ::    cntySW(:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyET(:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyRO(:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyDP(:,:,:)
    DOUBLE PRECISION, allocatable ::    cntySL(:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyPSL(:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyDAP(:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyETG(:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyDET(:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyETB(:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyDP1(:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyDP2(:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyRO1(:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyRO2(:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyEWAT(:,:,:)
    DOUBLE PRECISION, allocatable ::    cntySF(:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyRO2DP(:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyRO2ET(:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyETtrans(:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyNIRmET(:,:,:)
    
    ! County Irrigation Totals
    DOUBLE PRECISION, allocatable ::    cntyirrPrecip(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyirrAc(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyirrAP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyirrSW(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyirrET(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyirrRO(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyirrDP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyirrSL(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyirrPSL(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyirrDAP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyirrETG(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyirrDET(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyirrETB(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyirrDP1(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyirrDP2(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyirrRO1(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyirrRO2(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyirrEWAT(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyirrSF(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyirrRO2DP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyirrRO2ET(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyirrETtrans(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntyirrNIRmET(:,:,:,:)
    
    ! County Crop Totals
    DOUBLE PRECISION, allocatable ::    cntycrpPrecip(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntycrpAc(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntycrpAP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntycrpSW(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntycrpET(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntycrpRO(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntycrpDP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntycrpSL(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntycrpPSL(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntycrpDAP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntycrpETG(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntycrpDET(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntycrpETB(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntycrpDP1(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntycrpDP2(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntycrpRO1(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntycrpRO2(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntycrpEWAT(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntycrpSF(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntycrpRO2DP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntycrpRO2ET(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntycrpETtrans(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    cntycrpNIRmET(:,:,:,:)
    
    ! County Irrigation and Crop Totals
    DOUBLE PRECISION, allocatable ::    CICPrecip(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CICAc(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CICAP(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CICSW(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CICET(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CICRO(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CICDP(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CICSL(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CICPSL(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CICDAP(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CICETG(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CICDET(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CICETB(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CICDP1(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CICDP2(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CICRO1(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CICRO2(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CICEWAT(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CICSF(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CICRO2DP(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CICRO2ET(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CICETtrans(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CICNIRmET(:,:,:,:,:)
    
    !Runoff Zone totals
    DOUBLE PRECISION, allocatable ::    ROZPrecip(:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZAc(:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZAP(:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZSW(:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZET(:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZRO(:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZDP(:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZSL(:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZPSL(:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZDAP(:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZETG(:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZDET(:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZETB(:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZDP1(:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZDP2(:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZRO1(:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZRO2(:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZEWAT(:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZSF(:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZRO2DP(:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZRO2ET(:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZETtrans(:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZNIRmET(:,:,:)
    
    !Runoff Zone Irrigation totals
    DOUBLE PRECISION, allocatable ::    ROZirrPrecip(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZirrAc(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZirrAP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZirrSW(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZirrET(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZirrRO(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZirrDP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZirrSL(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZirrPSL(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZirrDAP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZirrETG(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZirrDET(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZirrETB(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZirrDP1(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZirrDP2(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZirrRO1(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZirrRO2(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZirrEWAT(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZirrSF(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZirrRO2DP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZirrRO2ET(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZirrETtrans(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZirrNIRmET(:,:,:,:)
    
    !Runoff Zone Crop Totals
    DOUBLE PRECISION, allocatable ::    ROZcrpPrecip(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZcrpAc(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZcrpAP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZcrpSW(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZcrpET(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZcrpRO(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZcrpDP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZcrpSL(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZcrpPSL(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZcrpDAP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZcrpETG(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZcrpDET(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZcrpETB(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZcrpDP1(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZcrpDP2(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZcrpRO1(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZcrpRO2(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZcrpEWAT(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZcrpSF(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZcrpRO2DP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZcrpRO2ET(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZcrpETtrans(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZcrpNIRmET(:,:,:,:)
    
    !Runoff Zone Irrigation and crop Totals
    DOUBLE PRECISION, allocatable ::    ROZiCPrecip(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZiCAc(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZiCAP(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZiCSW(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZiCET(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZiCRO(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZiCDP(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZiCSL(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZiCPSL(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZiCDAP(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZiCETG(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZiCDET(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZiCETB(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZiCDP1(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZiCDP2(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZiCRO1(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZiCRO2(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZiCEWAT(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZiCSF(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZiCRO2DP(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZiCRO2ET(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZiCETtrans(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    ROZiCNIRmET(:,:,:,:,:)
    
    !Coefficient Zone Totals
    DOUBLE PRECISION, allocatable ::    CoEZPrecip(:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZAc(:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZAP(:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZSW(:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZET(:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZRO(:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZDP(:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZSL(:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZPSL(:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZDAP(:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZETG(:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZDET(:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZETB(:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZDP1(:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZDP2(:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZRO1(:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZRO2(:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZEWAT(:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZSF(:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZRO2DP(:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZRO2ET(:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZETtrans(:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZNIRmET(:,:,:)
    
    !Coefficient Zone Irrgation Totals
    DOUBLE PRECISION, allocatable ::    CoEZirrPrecip(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZirrAc(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZirrAP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZirrSW(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZirrET(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZirrRO(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZirrDP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZirrSL(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZirrPSL(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZirrDAP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZirrETG(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZirrDET(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZirrETB(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZirrDP1(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZirrDP2(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZirrRO1(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZirrRO2(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZirrEWAT(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZirrSF(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZirrRO2DP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZirrRO2ET(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZirrETtrans(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZirrNIRmET(:,:,:,:)
    
    !Coefficient Zone Crop Totals
    DOUBLE PRECISION, allocatable ::    CoEZcrpPrecip(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZcrpAc(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZcrpAP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZcrpSW(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZcrpET(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZcrpRO(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZcrpDP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZcrpSL(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZcrpPSL(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZcrpDAP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZcrpETG(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZcrpDET(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZcrpETB(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZcrpDP1(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZcrpDP2(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZcrpRO1(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZcrpRO2(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZcrpEWAT(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZcrpSF(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZcrpRO2DP(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZcrpRO2ET(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZcrpETtrans(:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZcrpNIRmET(:,:,:,:)
    
    !Coefficient Zone Irrigaiton and Crop Totals
    DOUBLE PRECISION, allocatable ::    CoEZICPrecip(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZICAc(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZICAP(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZICSW(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZICET(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZICRO(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZICDP(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZICSL(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZICPSL(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZICDAP(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZICETG(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZICDET(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZICETB(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZICDP1(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZICDP2(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZICRO1(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZICRO2(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZICEWAT(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZICSF(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZICRO2DP(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZICRO2ET(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZICETtrans(:,:,:,:,:)
    DOUBLE PRECISION, allocatable ::    CoEZICNIRmET(:,:,:,:,:)

end module param2
!*************************************************************************
