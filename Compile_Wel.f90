!  Compile_Wel.f90 
!
!  FUNCTIONS:
!  Compile_Wel - Entry point of console application.
!

!****************************************************************************
!
!  PROGRAM: Compile_Wel
!
!  PURPOSE:  Entry point for the console application.
!
!****************************************************************************

subroutine Compile_Wel
    use Param
    IMPLICIT NONE
   
    
    CHARACTER*50    Line
   
    INTEGER, allocatable   ::  MLSP( : )
    INTEGER     src, row, col, mon, nday
   
    REAL    totpmp1, totpmp2
    
    
    WRITE (output_unit, '(A)') 'COMPWEL'
    
    allocate(MLSP(startyr : endyr))    
    DO iyear = startyr, endyr
        WRITE(YRT, '(I4)') iyear
        OPEN (iyear, FILE = TRIM(OUTDIR)//'WEL\'//TRIM(YRT)//'.WEL', STATUS = 'OLD')
        READ (iyear, *) MLSP(iyear)
    END DO
    
    MLPSP = MAXVAL(MLSP)
    
    call openoutfile(1, TRIM(OUTDIR), trim(proj_name)//'.WEL')
    WRITE (1, 100)
    WRITE (1, 101)
    WRITE (1, 102) MLPSP, 40
    
    call openoutfile(11, TRIM(OUTDIR), 'Well_Chk.txt')
    WRITE (11, 110)
    
    !WRITE (6, *) '.WEL Files'
    DO iyear = startyr, endyr
        WRITE(output_unit, '(I6, I8)') iyear, cyr(iyear)
        WRITE(YRT, '(I4)') iyear
        
        DO WHILE (.NOT. EOF(iyear))
            READ (iyear, 103) Line
            WRITE (1, 103) Line
        END DO
        
        
        OPEN (10, FILE = TRIM(OUTDIR)//'Well_chk\Well_chk'//TRIM(YRT)//'.txt', STATUS = 'OLD')
        READ (10, *)
        DO WHILE (.NOT. EOF(10))
            READ (10, *) i, layer, src, row, col, yr, mon, nday, totpmp1, totpmp2
            WRITE (11, 111) i, layer, src, row, col, yr, mon, nday, totpmp1, totpmp2
        END DO
        

    END DO
    
    CLOSE (1)
    
    DO iyear = startyr, endyr
        CLOSE (iyear)
    END DO
    CLOSE (11)
    
100 FORMAT ('#MODFLOW2000 Recharge Package')
101 FORMAT ('PARAMETER  0  0')
102 FORMAT (2I10)
103 FORMAT (A50)
    
110	FORMAT ('Cell, Layer, Well_Data_Source, Row, Column, Year, Month, NumberOfDaysInPeriod, Pumping_AFpD, Pumping_cfd')
111 FORMAT (I5, 2(', ', I1), ', ', 2(I3, ', '), I4, ', ', 2(I3, ', '), F12.7, ', ', F12.2)   
    
END subroutine Compile_Wel
!****************************************************************************